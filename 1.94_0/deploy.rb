#!/usr/bin/env ruby

puts "=== Creating vocus-extension deploy"

# make sure we are in the vocus-ext directory
current_dir = `pwd`.strip.split("/").last
if (current_dir != "vocus-ext")
    puts "[ERROR]: you need to be in 'vocus-ext' directory."
    exit()
end

# make sure VOCUS_DEBUG is false
constants = File.read("core/constants.js")

if (!constants.include?("VOCUS_IS_DEBUG = false"))
    puts "[ERROR]: VOCUS_IS_DEBUG is set to true."
    exit()
end

# all good? zip it
`zip -r ../vocus-ext.zip . -x *.git*`