var VocusSendlater = {
	timeInput: null,
	timeLabel: null,

	init: function()
	{
		// observe events
		PubsubClient.subscribe("compose-property-changed", VocusSendlater.checkProperty);
		PubsubClient.subscribe("snippets-expand", VocusSendlater.expandSnippet);
	},

	openPanel: function()
	{
		var dom = $($("#vocus-sendlater-template").html().trim());
		VocusCPanel.setContent(dom);

		// arrange
		VocusSendlater.timeInput = $("#vocus-sendlater-input");
		VocusSendlater.timeLabel = $("#vocus-sendlater-label");

		// load settings
		VocusSendlater.loadSettings();
		VocusSendlater.radioChanged();

		// bind actions
		VocusSendlater.timeInput.on("keyup", VocusSendlater.settingsChanged);
		$("#vocus-panel-done").on("click", VocusSendlater.saveSettings);
		$("#vocus-sendlater-now").on("change", VocusSendlater.radioChanged);
		$("#vocus-sendlater-later").on("change", VocusSendlater.radioChanged);
	},

	radioChanged: function()
	{
		var isNow = $("#vocus-sendlater-now").is(":checked");

		if (isNow)
		{
			$("#vocus-sendlater-result").addClass("disabled");
			VocusSendlater.timeInput.attr("disabled", "disabled");
			VocusSendlater.timeInput.val("");
			VocusSendlater.timeLabel.html("Immediate");
		}
		else
		{
			$("#vocus-sendlater-result").removeClass("disabled");
			VocusSendlater.timeInput.removeAttr("disabled");
		}
	},

	loadSettings: function()
	{
		var prop = VocusCompose.getProperty(null, "vocusSendlater");

		if (prop == "") {
			$("#vocus-sendlater-now").attr("checked", "checked");
		} else {
			$("#vocus-sendlater-later").attr("checked", "checked");
			var date = Date.create(prop);
			VocusSendlater.timeLabel.html(date.long());
			VocusSendlater.timeInput.val(date.long());
		}
	},

	saveSettings: function()
	{
		var human = VocusSendlater.timeInput.val().toLowerCase().trim();
		var when = VocusSendlater.timeLabel.html();

		// validate
		if (when.toLowerCase().indexOf("invalid") > -1)
		{
			alert("Please type in a valid date.");
			return;
		}

		// add sendlater property
		if (when == "Immediate")
		{
			VocusCompose.setProperty(null, "vocusSendlater", "");
		}
		else
		{
			VocusCompose.setProperty(null, "vocusSendlater", Date.create(when).toISOString());
		}

		VocusCPanel.hide();
	},

	settingsChanged: function()
	{
		// translate human to actual
		var human = VocusSendlater.timeInput.val().toLowerCase().trim();

		// now or disabled
		if (human == "" || human == "now")
		{
			VocusSendlater.timeLabel.html("Immediate");
		}

		// later
		else
		{
			var parsed = Date.future(human).long();
			VocusSendlater.timeLabel.html(parsed);
		}
	},

	remove: function()
	{
		VocusCompose.setProperty(null, "vocusSendlater", "");
	},

	checkProperty: function(changes)
	{
		// show Sendlater+Followup note
		var sendlaterProp = VocusCompose.getProperty(null, "vocusSendlater");
		var followupProp = VocusCompose.getProperty(null, "vocusFollowup");

		if (sendlaterProp != "" && followupProp != "")
		{
			VocusCompose.addNote({
				name: "sendlaterFollowup", 
				body: "Heads up! Sendlater and Followup dates are independent - <a href='https://support.vocus.io/article/45-understanding-followups-and-send-later-timing' target='_blank'>learn more</a>."
			});
		}
		else
		{
			VocusCompose.removeNote({name: "sendlaterFollowup"});
		}

		// show Sendlater note
		if (changes.propName != "vocusSendlater")
			return;
		
		if (changes.context && changes.context != VocusFeatures.context)
			return;

		if (changes.propValue == "")
		{
			VocusCompose.removeNote({name: "sendlater"});
		}
		else
		{
			VocusCompose.addNote({
				name: "sendlater", 
				body: "After clicking Send, Vocus.io will send message @ " + Date.create(changes.propValue).toLocaleString(),
				linkLabel: "remove",
				linkHandler: VocusSendlater.remove
			});
		}
	},

	expandSnippet: function(snippet)
	{
		if (snippet.sendlaterTime == null || snippet.sendlaterTime == "")
			return;
		
		var humanTime = snippet.sendlaterTime.toLowerCase().trim();
		var actualTime = Date.future(humanTime);

		if (Date.create() >= actualTime)
		{
			alert("Failed to schedule email: the time value '" + humanTime + "' is not relative, already passed.");
			return;
		}

		VocusCompose.setProperty(null, "vocusSendlater", actualTime.toISOString());
	}
}

// $(VocusSendlater.init);