/*
*	TODO: with the release of Dashboard-v2, no need to keep track of (VocusDashboard.isVisible) and 
*	close() and refresh() methods. This file can likely be simplified to keep init() only. We will also
*	need to search for VocusDashboard.isVisible, VocusDashboard.refresh, VocusDashboard.close, etc and remove
* 	any references to those, such as the many references in toast.js.
*/

var VocusDashboard = {
	isVisible: false,

	init: function() {
		// create button
		VocusFeatures.iSDK.Toolbars.addToolbarButtonForApp({
			iconUrl: VOCUS_IMAGES_PATH + 'icon-letter-only.png',
			titleClass: 'vocus-toolbar-button-title',
			iconClass: 'vocus-toolbar-button-class',

			onClick: function(btn) { 
				btn.dropdown.close();

				var email = VocusFeatures.getEmailAddress();

				if (!VocusFeatures.isAccount(email)) {
					VocusFeatures.dialog({header: "Activate Vocus.io", content: `This email inbox is not configured to run on Vocus.io. To activate, click <a target='_blank' href='https://vocus.io/add_inbox'>here</a> and sign in as (${VocusFeatures.getEmailAddress()}).`, hideAlert: true});	
					return;
				}
				
				VocusDashboard.open();
			}

		});

		// accept requests (such as those coming from Web)
		PubsubClient.subscribe("open-dashboard", function(params) {
			if (VocusFeatures.enableNewDashboard())
			{
				return; // with new dashboard, no need to refresh things all the time
			}

			if (params.url)
			{
				VocusDashboard.open(params.url);
			}
			else
			{
				VocusDashboard.open();
			}

			if (VocusDashboard.isVisible)
			{
				PubsubClient.publish("frame-hash-changed", {});
			}
		});

		PubsubClient.subscribe("close-dashboard", function() {
			VocusDashboard.close();
		});
	},

	open: function(url) {

		if (VocusFeatures.enableNewDashboard())
		{
			url = VOCUS_SERVER + (typeof url == "string" ? url : "/dashboard")
			window.open(url);
			return;
		}

		// make visible
		VocusDashboard.isVisible = true;

		VocusPopup.open({
			width: 850, 
			height: 500, 
			url: (typeof url == "string" ? url : "/dashboard?version=" + VOCUS_VERSION)
		});
	},

	close: function() {
		VocusDashboard.isVisible = false;
		VocusPopup.close();
	},

	refresh: function() {
		if (!VocusDashboard.isVisible)
			return;

		PubsubClient.publish("frame-hash-changed");
	},
}