var VocusCompose =
{
	init: function()
	{
		// keep a lookout for new/dead compose windows
		VocusFeatures.iSDK.Compose.registerComposeViewHandler(VocusCompose.onNewCompose);

		// observe draft-save events
		PubsubClient.subscribe("draft-saving", VocusCompose.onDraftSaving);
		PubsubClient.subscribe("draft-saved", VocusCompose.onDraftSaved);
		
		// observe slice events
		PubsubClient.subscribe("insert-slice", VocusCompose.insertSlice);
		PubsubClient.subscribe("prepend-slice", VocusCompose.prependSlice);
		PubsubClient.subscribe("remove-slice", VocusCompose.removeSlice);
	},

	//
	// Events

	onNewCompose: function(view)
	{
		// get the parent box
		var box = $(view.getBodyElement()).parents(".M9");

		// mark it as "enahnced by vocus"
		if (box.attr("data-vocus-enhanced") == "true")
			return;
		
		box.attr("data-vocus-enhanced", "true");
		box.data("view", view);

		// insert vocus account tag
		VocusCompose.setProperty(box, "vocusAccount", VocusFeatures.iSDK.User.getEmailAddress());
		
		// switch indicator on or off
		var isEnabled = true;
		if (!VocusFeatures.auth) isEnabled = false; // not authorized
		if (!VocusFeatures.settings.defaultVocusEnabled) isEnabled = false; // default to disable
		if (VocusFeatures.profile.disabled) isEnabled = false; // account expired (or other reasons)

		VocusCPanel.setVocusSwitchState(box, isEnabled);
		VocusTracking.initTrackingSettings(box);

		// listen for further events
		view.on("destroy", VocusCompose.onDestroyCompose);
		view.on("minimized", VocusCompose.onHideCompose);
		view.on("toContactAdded", VocusCompose.onContactAdded);

		// mark it as clean, attach events for dirty/clean
		VocusCompose.setDirty(box, false);

		view.on("recipientsChanged", () => {
			VocusCompose.setDirty(box, true);
		});

		$(box).find("input[name='subjectbox']").on("input", () => {
			VocusCompose.setDirty(box, true);
		});

		$(box).find("div[contenteditable][role=textbox]").on("input", () => {
			VocusCompose.setDirty(box, true);
		});

		// save draft and init vocus tracker on-send
		view.on("presending", function(event) {
			VocusTracking.onEmailPresending(view, event);
		});
	},

	onDestroyCompose: function(view)
	{
		VocusCompose.setState("hidden");
	},

	onHideCompose: function(view)
	{
		VocusCompose.setState("hidden");
	},

	setState: function(name)
	{
		PubsubClient.publish("compose-state-changed");
		PubsubClient.publish("compose-" + name);
	},

	onContactAdded: function(obj)
	{
		// show warning if sending email to self
		if (obj.contact.emailAddress.toLowerCase() == VocusFeatures.iSDK.User.getEmailAddress().toLowerCase())
		{
			VocusCompose.addNote({name: "self-warning", body: "Emails to yourself will skip Vocus.io filter - <a href='https://vocus.io/how-to-test-vocus' target='_blank'>learn more</a>"});
		}
		else
		{
			VocusCompose.removeNote({name: "self-warning"});
		}
	},

	//
	// Draft "dirty" / "clean" helpers

	setDirty(box, flag)
	{
		if (VocusCompose.isDirty(box) != flag.toString())
		{
			VocusCompose.setProperty(box, "vocusIsDirty", flag);
		}
	},

	isDirty: function(box)
	{
		return VocusCompose.getProperty(box, "vocusIsDirty");
	},

	onDraftSaving: function(obj)
	{

	},

	onDraftSaved: function(obj)
	{
		if (obj == null)
			return;

		// get the right box
		var box = null;

		if (obj.msgId)
		{
			box = VocusCompose.getBoxFromMsgId(obj.msgId);
		}
		else if (obj.threadId)
		{
			box = VocusCompose.getBoxFromThreadId(obj.threadId);
		}

		// if no matching box found, then stop
		if (box == null || box.length == 0)
			return;

		// mark msg as clean when saved
		VocusCompose.setDirty(box, false);
	},

	forceDraftSave: function(box)
	{
		// set as dirty
		VocusCompose.setDirty(box, true);

		// force save
		// at the moment we use InboxSDK API to trigger this, but we need an independent solution
		var view = box.data("view");
		var body = view.getHTMLContent();
		view.setBodyHTML(body + " ");
	},

	//
	// Compose manipulation (LEGACY)

	getAllCompose: function()
	{
		return $(".M9");
	},

	getCurrentCompose: function()
	{
		var found = [];
		var all = VocusCompose.getAllCompose();

		all.each(function(i, elem) {
			var dom = $(elem);

			if (dom.height() > 0)
				found.push(dom);
		});

		// nothing found? return null
		if (found.length == 0)
		{
			return null;
		}

		// only one box? return it
		else if (found.length == 1)
		{
			return found[0]
		}

		// more than one box? return the one which has focus
		else
		{
			for(var i = 0; i < found.length; i++)
			{
				var dom = found[i];

				if (dom.has(":focus").length > 0)
					return dom;
			}

			return found[found.length - 1];
		}
	},

	//
	// Note operations

	addNote: function(options)
	{
		// validate note
		if (options.context && options.context != VocusFeatures.context)
			return;

		// get it
		var box = VocusCompose.getCurrentCompose();
		var note = box.find(".vocus-compose-note[data-vocus-note='" + options.name + "']");

		if (note.length == 0)
		{
			note = $("<div class='vocus-compose-note'></div>");
			note.attr("data-vocus-note", options.name);
		}

		// set body
		note.html(options.body);

		// make links
		if (options.linkLabel)
		{
			var link = $("<a href='#'>" + options.linkLabel + "</a>");
			link.on("click", options.linkHandler);
			note.append(" - ").append(link);
		}

		// append
		box.find(".Ar.Au").prepend(note);
	},

	removeNote: function(options)
	{
		if (options.context && options.context != VocusFeatures.context)
			return;

		var box = VocusCompose.getCurrentCompose();
		var note = box.find(".vocus-compose-note[data-vocus-note='" + options.name + "']");
		note.remove();
	},

	//
	// Property operations

	setProperty: function(box, name, value)
	{
		if (box == null)
			box = VocusCompose.getCurrentCompose();
		
		var label = box.find("input[type='hidden'][name='" + name + "']");

		if (label.length == 0) {
			label = $("<input type='hidden' name='" + name + "' value='' />");
			box.find("input[type='hidden'][name='composeid']").before(label);
		}

		label.val(value);

		// publish with id
		var id = box.attr("id");

		if (!id)
		{
			id = "vocus-box-" + Math.round(Math.random()*1000000000);
			box.attr("id", id);
		}

		PubsubClient.publish("compose-property-changed", {context: VocusFeatures.context, boxId: id, propName: name, propValue: value});
	},

	getProperty: function(box, name)
	{
		if (box == null)
			box = VocusCompose.getCurrentCompose();
		
		var label = box.find("input[type='hidden'][name='" + name + "']");

		if (label.length == 0)
			return "";

		return label.val();
	},

	//
	// Slice (HTML injection) operations

	insertSlice: function(options)
	{
		// validate context if exists
		if (options.context && options.context != VocusFeatures.context)
			return;

		// give focus
		var box = VocusCompose.getCurrentCompose();
		box.find("div.Am.Al.editable").focus();
		
		// arrange
		var range = getSelection().getRangeAt(0);
		var clone = range.cloneRange();

		// insert expansion
		var node = $("<div>" + options.slice + "</div>");

		clone.insertNode(node[0]);
		VocusCompose.forceDraftSave(box);
	},

	prependSlice: function(options)
	{
		// validate context if exists
		if (options.context && options.context != VocusFeatures.context)
			return;

		var box = VocusCompose.getCurrentCompose();
		// var body = box.find("div[aria-label='Message Body']");
		var body = box.find("div.Am.Al.editable");
		var content = body.html() == "" ? "<br />" : body.html();
		body.html("<div>" + content + "</div>");
		body.prepend(options.slice);
	},

	removeSlice: function(options)
	{
		if (options.context && options.context != VocusFeatures.context)
			return;

		var box = VocusCompose.getCurrentCompose();
		// var body = box.find("div[aria-label='Message Body']");
		var body = box.find("div.Am.Al.editable");
		var slice = body.find("#" + options.sliceId);
		slice.remove();
	},

	//
	// Misc.

	openDraft: function(id)
	{
		window.location.hash = "#inbox?compose=" + id;
	},

	getBoxFromMsgId: function(msgId)
	{
		return $("input[value*='" + msgId + "'").parents(".M9");
	},

	getBoxFromThreadId: function(threadId)
	{
		return $("input[value*='" + threadId + "'").parents(".M9");
	}
}