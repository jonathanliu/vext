var VocusSnippets =
{
	snippets: [],
	varbox: null,
	variables: null,
	activeSnippet: null,

	init: function()
	{
		VocusSnippets.refresh();

		// arrange
		VocusSnippets.varbox = $("#vocus-snippet-varbox");
		VocusSnippets.varboxModal = $("#vocus-snippet-varbox-modal");
		VocusSnippets.varboxModal.click(VocusSnippets.hideVarbox);

		// observe changes
		PubsubClient.subscribe("snippets-refresh", VocusSnippets.refresh);
		PubsubClient.subscribe("session-started", VocusSnippets.refresh);

		// observe clicks on vars
		$(document).on("click", ".vocus-snippet-variable", VocusSnippets.variableClicked);
	},

	initCommands: function()
	{
		VocusCompletion.resetCommands("VocusSnippets");

		for(var name in VocusSnippets.snippets)
			VocusCompletion.addCommand("VocusSnippets", name, VocusSnippets.expand);
	},

	expand: function(name)
	{
		var snippet = underscore.find(VocusSnippets.snippets, function(s) {
			return s.name == name;
		});

		if (snippet == null)
			return "<span>Error loading snippet. If this persists, please reach out to the Vocus.io team hello@vocus.io</span>";

		var body = atob(snippet.body);

		// decode unicode
		if (snippet.unicode)
		{
			body = decodeURIComponent(body);
		}
		
		// apply subject
		if (snippet.subject != null && snippet.subject.trim() != "")
		{
			var subjectBox = VocusCompose.getCurrentCompose().find("input[name=subjectbox]");

			if (subjectBox.length > 0)
				subjectBox.val(snippet.subject);
		}

		// replace magic variables
		if (body.length > 0)
		{
			body = body.replace(/\[\[Sender Name\]\]/gi, VocusFeatures.profile.name);
			body = body.replace(/\[\[Sender First Name\]\]/gi, VocusFeatures.profile.name.split(" ")[0]);
		}

		// replace normal variables
		if (body.length > 0)
		{
			var bodyVars = body.match(/\[\[(.+?)\]\]/g) || [];
			var subjectVars = (snippet.subject != null) ? (snippet.subject.match(/\[\[(.+?)\]\]/g) || []) : [];
			var variables = underscore.union(bodyVars, subjectVars);

			VocusSnippets.variables = {};

			for (var i = 0; i < variables.length; i++) {
				var name = variables[i];
				body = body.split(name).join("<span class='vocus-snippet-variable' data-vocus-variable='" + name + "'>" + name + "</span>");
				VocusSnippets.variables[name] = name;
			}

			if (variables.length > 0)
			{
				VocusSnippets.showVarbox();
			}
		}

		// inform snippet-expand observers (snooze, sendlater)
		VocusSnippets.activeSnippet = snippet;
		PubsubClient.publish("snippets-expand", snippet);

		// inform snippet-form observers (followups) only if no varbox expected
		if (Object.keys(VocusSnippets.variables).length == 0)
		{
			PubsubClient.publish("snippets-form-submit", {snippet: VocusSnippets.activeSnippet, variables: VocusSnippets.variables});
		}

		return "<span>" + body + "</span>&nbsp;";
	},

	refresh: function()
	{
		console.log("[Vocus Snippets] Refreshing snippets.");
		
		PubsubClient.async("VocusDaemon", "getHTTP", [VOCUS_SERVER + "/snippets/list", null], function(result)
		{
			VocusSnippets.snippets = result;
			VocusSnippets.bind();
		})
	},

	bind: function()
	{
		VocusCompletion.resetCommands("VocusSnippets");

		for(var i = 0; i < VocusSnippets.snippets.length; i++)
		{
			var s = VocusSnippets.snippets[i];
			VocusCompletion.addCommand("VocusSnippets", s.name, VocusSnippets.expand);
		}
	},

	openPanel: function()
	{
		var items = [];

		for(var i = 0; i < VocusSnippets.snippets.length; i++)
		{
			var s = VocusSnippets.snippets[i];
			var item = $("<div class='vocus-panel-item'>" + s.name + "</div>");
			item.on("click", VocusSnippets.panelClicked);
			items.push(item);
		}

		VocusCPanel.setContent(items);
	},

	panelClicked: function(e)
	{
		var name = $(e.target).text();
		var content = VocusSnippets.expand(name);

		VocusCompletion.giveFocusBack();
		VocusCompose.insertSlice({slice: content});
		VocusCPanel.hide();
	},

	/*** Varbox ***/
	showVarbox: function()
	{
		// get compose box position
		var box = VocusCompose.getCurrentCompose();
		var rects = box.get(0).getClientRects()[0];
		
		// set modal position and show
		VocusSnippets.varboxModal.css({top: rects.top, left: rects.left, width: rects.width, height: rects.height});
		VocusSnippets.varboxModal.show();

		// populate varbox fields
		var html = $("<div></div>");

		// for (var i = 0; i < VocusSnippets.variables.length; i++) {
		for (var key in VocusSnippets.variables) {
			var humanName = key.replace(/\[/g, "").replace(/\]/g, "");
			var input = $("<input type='text' name='" + key + "' placeholder='" + humanName + "' />");
			input.keyup(VocusSnippets.variableChanged);
			html.append(input);
		}

		VocusSnippets.varbox.html(html);

		// set varbox position and show
		VocusSnippets.varbox.css({
			top: 	rects.top + rects.height / 2 - VocusSnippets.varbox.outerHeight() / 2 - 50,
			left: 	rects.left + rects.width / 2 - VocusSnippets.varbox.outerWidth() / 2
		});

		VocusSnippets.varbox.show();

		// set subject line in data attr (needed for real-time changes)
		var subject = box.find("input[name=subjectbox]");
		subject.attr("data-vocus-subject", subject.val());

		// auto focus on first text field
		setTimeout(function() {
			VocusSnippets.varbox.find("input:first").focus();
		}, 300);
	},

	hideVarbox: function()
	{
		VocusSnippets.varbox.hide();
		VocusSnippets.varboxModal.hide();

		// post-process variables
		var box = VocusCompose.getCurrentCompose();

		$(".vocus-snippet-variable").each(function(i, elem) {
			var obj = $(elem);
			var name = obj.attr("data-vocus-variable");
			var value = obj.html();
			
			// if value specified, then unhighlight that variable
			if (name != value)
			{
				obj.removeClass("vocus-snippet-variable");
			}
		});

		// inform observers
		PubsubClient.publish("snippets-form-submit", {snippet: VocusSnippets.activeSnippet, variables: VocusSnippets.variables});

		// force save draft
		VocusCompose.forceDraftSave(box);
	},

	getVarboxValuePairs: function()
	{
		// returns hash with variable names as keys and their values as populated values in the box
		var output = {};

		VocusSnippets.varbox.find("input").each(function(i, elem) {
			var obj = $(elem);
			output[obj.attr("name")] = obj.val();
		});

		return output;
	},

	variableChanged: function(e)
	{
		// enter key? hide box
		if (e.which == 13)
		{
			VocusSnippets.hideVarbox();
			return;
		}

		// get box and variable name/val
		var box = VocusCompose.getCurrentCompose();
		var input = $(this);
		var name = input.attr("name");
		var value = input.val();

		if (value == "")
			value = name;

		// update vars in message body
		var body = box.find("div.Am.Al.editable");
		var field = body.find("span[data-vocus-variable='" + name + "']");
		field.html(value);

		// update vars in subject line
		var subjectObj = box.find("input[name=subjectbox]");
		var subjectVal = subjectObj.attr("data-vocus-subject");
		var pairs = VocusSnippets.getVarboxValuePairs();

		for (var key in pairs) {
			if (pairs[key] == "") continue;
			subjectVal = subjectVal.replace(key,pairs[key]);
		}

		subjectObj.val(subjectVal);

		// update global vars
		VocusSnippets.variables[name] = value;
	},

	variableClicked: function(e)
	{
		var field = $(e.target);
		var name = field.attr("data-vocus-variable");
		VocusSnippets.showVarbox([name]);
	}
};

// setTimeout(VocusSnippets.init, 3000);