var VocusDrafts =
{
	hiddenDrafts: [],

	init: function()
	{
		// observe Draft boxes
		DomObserver.attach("body", {childList: true }, VocusDrafts.onBodyChange);
		
		// listen to Row changes
		VocusFeatures.iSDK.Lists.registerThreadRowViewHandler(VocusDrafts.onRowHandler);
	},
	
	//
	// onBodyChange / onRowHandler

	// Needed when using AdvancedTracking, needed to hide drafts until next refresh
	onBodyChange: function()
	{
		// nothing to hide? do nothing
		if (VocusDrafts.hiddenDrafts.length == 0) return;

		// no reply box? do nothing
		var fields = $("input[name=draft]");
		if (fields.length == 0) return;

		// found matching box? hide it
		fields.each(function() {
			var elem = $(this);
			var draftId = elem.val();
			var match = VocusDrafts.shouldHideDraft(draftId);

			if (match)
			{
				elem.parents(".M9").hide();
			}
		});
	},

	onRowHandler: function(row)
	{
		// nothing to hide? do nothing
		if (VocusDrafts.hiddenDrafts.length == 0) return;

		// has no draft? do nothing
		if (row.getVisibleDraftCount() <= 0) return;

		// has draft? check ID, hide if matched
		var threadId = row.getThreadID();
		var match = VocusDrafts.shouldHideThread(threadId);

		if (match)
		{
			row.replaceDraftLabel({});
		}
	},

	//
	// hide/clear draft

	hideDraft: function(box)
	{
		box.hide();
		
		// add it to list
		VocusDrafts.hiddenDrafts.push({
			draftId: box.find("input[name=draft]").val(),
			threadId: box.find("input[name=lts]").val()
		});
	},

	clearHiddenDrafts: function(instant)
	{
		// allow some time before you clear it, except in Errors
		delay = instant ? 0 : 1500;

		setTimeout(function() {
			VocusDrafts.hiddenDrafts = [];
		}, delay);
	},
	
	//
	// should hide draft/thread

	shouldHideDraft: function(draftId)
	{
		return underscore(VocusDrafts.hiddenDrafts).any((i) => i.draftId == draftId);
	},

	shouldHideThread: function(threadId)
	{
		return underscore(VocusDrafts.hiddenDrafts).any((i) => i.threadId == threadId);
	}
}