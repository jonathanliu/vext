var VocusToast =
{
	disabledToasts: [],

	init: function()
	{
		// arrange
		VocusToast.dom = $("#vocus-toast");

		// listen
		PubsubClient.subscribe("add-toast", VocusToast.add);
		PubsubClient.subscribe("remove-toast", VocusToast.remove);
		PubsubClient.subscribe("profile-changed", VocusToast.profileChanged);
		PubsubClient.subscribe("frame-visibility-changed", VocusToast.frameStateChanged);

		// get current configs
		PubsubClient.call("VocusDaemon", "sync");
		PubsubClient.get("VocusDaemon", "profile", VocusToast.profileChanged);
	},
	
	add: function(tag, message, hide)
	{
		// make sure it wasn't manually hidden
		if (VocusToast.disabledToasts.indexOf(tag) > -1)
			return;

		VocusToast.remove(tag);

		var msg = $("<p data-tag='" + tag + "'>" + message + "</p>");

		if (hide)
		{
			var hide = $("<span> (<a class='vocus-toast-hide'>hide</a>)</span>");

			hide.find("a").click(function() {
				VocusToast.remove(tag);
				VocusToast.disabledToasts.push(tag);

				if (tag == "multi-acc")
				{
					localStorage.setItem("notifyActivation", "false");
				}
			});

			msg.append(hide);
		}

		VocusToast.dom.append(msg);
		VocusToast.dom.show();
	},

	remove: function(tag)
	{
		var msg = VocusToast.dom.find("p[data-tag='" + tag + "']");
		msg.remove();

		if (VocusToast.dom.find("p").length == 0)
		{
			VocusToast.dom.hide();
		}
	},

	profileChanged: function(profile)
	{
		// debug?
		if (VOCUS_IS_DEBUG)
		{
			VocusToast.add("debug","You're running Vocus.io in debug mode.");
		}

		// signed out?
		if (VocusDashboard.isVisible == false && profile.auth == false)
		{
			VocusToast.add("auth", "You've been signed out from Vocus.io - click <a target='_blank' href='https://vocus.io/install'>here</a> to sign in.");
		}
		else
		{
			VocusToast.remove("auth");
		}

		// message?
		if (profile.serverMessage && profile.auth == true)
		{
			VocusToast.add("server-msg", profile.serverMessage);
		}
		else
		{
			VocusToast.remove("server-msg");
		}

		// different account? sign them out
		var email = VocusFeatures.getEmailAddress();
		var activated = VocusFeatures.isAccount(email);
		var notify = localStorage.getItem("notifyActivation") || "true";

		if (profile.auth == true && email != "" && !activated && notify == "true")
		{
			VocusToast.add("multi-acc", "Vocus.io was not activated for this inbox - <a target='_blank' href='https://vocus.io/add_inbox'>Add it</a>. ", true);
		}
		else
		{
			VocusToast.remove("multi-acc");
		}
	},

	frameStateChanged: function()
	{
		if (VocusDashboard.isVisible == true)
		{
			VocusToast.remove("auth");
			return;
		}

		PubsubClient.get("VocusDaemon", "profile", VocusToast.profileChanged);
	}
};