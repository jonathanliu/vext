var VocusTracking = {

	trackEmail: true,
	trackLinks: true,

	sendAdvancedErrorMessage: {
		header: "Something went wrong", 
		content: "Vocus.io failed to process your email. A copy of your email is saved in your Drafts folder in case you want to try to send it again. Refresh Gmail and try again. <br /><br />Follow <a href='https://vocus.io/troubleshoot-email-sending' target='_blank'>troubleshooting instructions</a> if the problem persists.<br />",
		hideAlert: true
	},

	init: function()
	{
		
	},

	getTrackingType: function(view, params)
	{
		// scheduling message? always use 'advanced', because sending it via 'classic' would immediately deliver it.
		if (params.sendlater != "") return "advanced";

		// inserted a poll or meeting box? then force server-side processing.
		if (view.getHTMLContent().indexOf("<input type=\"hidden\" name=\"vocus-blob\"") > -1) return "advanced";
		
		// otherwise, use common logic
		if (VocusFeatures.settings.trackingType == "auto")
		{
			// single contact? then use classic
			var contacts = view.getToRecipients().add(view.getCcRecipients());
			if (contacts.length <= 1) return "classic";

			// multi contact? then use advanced
			return "advanced";
		}
		
		return VocusFeatures.settings.trackingType;
	},

	//
	// Email Sending Events

	onEmailPresending: function(view, event)
	{
		// get settings
		var params = VocusTracking.getComposeSettings(view);

		// make sure we are supposed to track this
		if (!VocusFeatures.auth) return; // not authorized
		if (VocusFeatures.profile.disabled) return; // account expired (or other reasons)
		if (!params.enabled) return; // user disabled vocus for this single email

		// delegate to advanced-or-classic tracking
		var trackingType = VocusTracking.getTrackingType(view, params);

		if (trackingType == "advanced")
		{
			VocusTracking.processAdvancedTracking(view, event, params);
		}
		else
		{
			VocusTracking.processClassicTracking(view, event, params);
		}
	},

	//
	// Tracking Panel & Settings

	initTrackingSettings: function(box)
	{
		var settings = VocusCompose.getProperty(box, "vocusTracking");
		
		if (settings == "" && VocusFeatures.settings != null) {
			if (VocusFeatures.settings.defaultEmailTrack) settings = "email";
			if (VocusFeatures.settings.defaultLinkTrack) settings += ",links";
			VocusCompose.setProperty(box, "vocusTracking", settings);
		}
	},

	openPanel: function()
	{
		// prepare HTML
		var dom = $($("#vocus-tracking-template").html().trim());
		VocusCPanel.setContent(dom);

		// reflect current settings
		VocusTracking.loadSettings(null);
		$("#vocus-tracking-email").attr("checked", (VocusTracking.trackEmail ? true : false));
		$("#vocus-tracking-links").attr("checked", (VocusTracking.trackLinks ? true : false));

		// bind inputs
		$("#vocus-tracking input").on("change", VocusTracking.settingsChanged);

		// set defaults button
		$("#vocus-tracking-defaults span").on("click", VocusTracking.saveDefaults);
	},

	loadSettings: function(box)
	{
		if (box == null)
			box = VocusCompose.getCurrentCompose();

		var settings = VocusCompose.getProperty(box, "vocusTracking");

		VocusTracking.trackEmail = (settings.indexOf("email") > -1);
		VocusTracking.trackLinks = (settings.indexOf("links") > -1);
	},

	saveSettings: function()
	{
		var settings = [];
		if (VocusTracking.trackEmail) settings.push("email");
		if (VocusTracking.trackLinks) settings.push("links");
		if (settings.length == 0) settings.push("none");

		VocusCompose.setProperty(null, "vocusTracking", settings.join(","));
	},

	settingsChanged: function()
	{
		var box = VocusCompose.getCurrentCompose();
		var trackEmail = $("#vocus-tracking-email");
		var trackLinks = $("#vocus-tracking-links");

		VocusTracking.trackEmail = trackEmail.is(":checked");
		VocusTracking.trackLinks = trackLinks.is(":checked");

		VocusTracking.saveSettings();
	},

	saveDefaults: function()
	{
		VocusAlert.alert({content: "Saving defaults..."});

		// collect params
		var params = {
			account: VocusFeatures.iSDK.User.getEmailAddress(),
			trackEmails: VocusTracking.trackEmail,
			trackLinks: VocusTracking.trackLinks
		};

		// make call
		var url = VOCUS_SERVER + "/tracker/set_defaults";

		PubsubClient.async("VocusDaemon", "postHTTP", [url, params], function(result) {
			if (!result.success)
			{
				VocusAlert.alert({content: "Something went wrong while saving your settings.",seconds: 8});
				return;
			}

			VocusAlert.alert({content: "Saved - new settings will be applied to all future emails.",seconds: 8});
			PubsubClient.call("VocusDaemon", "sync");
		});
	},

	//
	// Advanced Tracking

	processAdvancedTracking: function(view, event, params)
	{
		// get box
		var box = $(view.getBodyElement()).parents(".M9");

		// determine endpoint to hit (sendlater versus normal)
		var url = VOCUS_SERVER + "/tracker/create_advanced";
		var alertProcessing = "Sending message...";

		if (params.sendlater != "")
		{
			url = VOCUS_SERVER + "/sendlater/create";
			alertProcessing = "Scheduling message...";
		}

		// tell user we're working on it
		VocusFeatures.alert({content: alertProcessing});

		// prevent normal send
		event.cancel();

		// dirty? force save
		if (params.isDirty)
		{
			console.log("[Vocus Tracking] Forcing draft save...");
			VocusCompose.forceDraftSave(box);
			
			setTimeout(function() {
				// update isDirty
				params.isDirty = (VocusCompose.getProperty(box, "vocusIsDirty") == "true");

				// post it
				VocusTracking.postAdvancedTracking(view, box, url, params);
			}, 1500);
		}

		// not dirty? send away
		else
		{
			VocusTracking.postAdvancedTracking(view, box, url, params);
		}
		
		
	},

	postAdvancedTracking: function(view, box, url, params)
	{
		// hide box
		if (view.isInlineReplyForm())
		{
			VocusDrafts.hideDraft(box);
		}
		else
		{
			view.close();
		}

		// get draft id (returned as a Promise)
		view.getDraftID().then(function(draftId) {
			
			// add to params
			params.draftId = draftId;
			params.newGmail = VocusFeatures.iSDK.User.isUsingGmailMaterialUI();

			// post it!
			PubsubClient.async("VocusDaemon", "postHTTP", [url, params], function(data) {

				// success
				if (data.success)
				{
					if (!params.newGmail && view.isInlineReplyForm())
					{
						VocusFeatures.forceRenderUI();
					}

					setTimeout(function() { 
						VocusFeatures.refresh();
						VocusFeatures.alert({content: data.message, seconds: 7});
					}, 1000);
				}

				// custom error
				else if (data.error_title)
				{
					VocusDrafts.clearHiddenDrafts(true);
					VocusFeatures.dialog({header: data.error_title, content: data.error_body, hideAlert: true});
					console.log("[VOCUS] Handled error (done) processing advanced tracking.");
					
					if (!view.isInlineReplyForm())
					{
						VocusFeatures.openLastDraft();
						VocusFeatures.openDraft(view.getDraftID());
					}
				}

				// random error
				else
				{
					VocusDrafts.clearHiddenDrafts(true);
					VocusFeatures.dialog(VocusTracking.sendAdvancedErrorMessage);
					console.log("[VOCUS] Unhandled error processing advanced tracking.");
					
					if (!view.isInlineReplyForm())
					{
						VocusFeatures.openLastDraft();
						VocusFeatures.openDraft(view.getDraftID());
					}

					try
					{
						console.log(data)
					}
					catch(err)
					{
						console.log(err)
					}
				}

				PubsubClient.call("VocusDaemon", "sync", {});
			});

		});
	},

	//
	// Classic Tracking

	processClassicTracking: function(view, event, params)
	{
		// make pixel
		var uuid = VocusTracking.getUUID();
		var url = VocusFeatures.settings.trackingEndpoint + "/hello.png?email=" + VocusFeatures.profile.email + "&id=" + uuid + "&v=1&type=classic";
		var img = "<div><img id='vocus-classic-tracker' src='" + url + "' style='display:none!important;' alt='vcs' /></div>";
		params.uuid = uuid;

		// get email content
		var html = view.getHTMLContent();

		// wrap links
		params.anchors = []

		if (params.tracking.indexOf("links") > -1)
		{
			var output = VocusTracking.getWrappedAnchors(html);
			html = output.html;
			params.anchors = output.anchors;
		}

		// insert pixel
		if (params.tracking.indexOf("email") > -1)
		{
			html += img;
		}

		// set new body
		view.setBodyHTML(html);

		// inform remote server
		view.on("sent", function(event) {
			event.getMessageID().then((id) => {

				// append additional metadata to params
				try { params.messageId = id; } 													catch(e) { console.log("[VOCUS] Error getting params (messageId) metadata on classic create.", e); }
				try { params.threadId = view.getThreadID(); } 									catch(e) { console.log("[VOCUS] Error getting params (threadId) metadata on classic create.", e); }
				try { params.newGmail = VocusFeatures.iSDK.User.isUsingGmailMaterialUI(); } 	catch(e) { console.log("[VOCUS] Error getting params (newGmail) metadata on classic create.", e); }
				// try { params.fromEmail = view.getFromContact().emailAddress; } 					catch(e) { console.log("[VOCUS] Error getting params (fromEmail) metadata on classic create.", e); }
				
				setTimeout(() => {
					VocusTracking.postClassicTracking(view, params);
				}, 2000);

			});
		});
	},

	postClassicTracking: function(view, params)
	{
		var url = VOCUS_SERVER + "/tracker/create_classic";

		PubsubClient.async("VocusDaemon", "postHTTP", [url, params], function(data) {

			if (data.success)
			{
				// good!
			}

			// something went wrong?
			else
			{
				console.log("[VOCUS] Handled error (done) processing classic tracking.");
				console.log(data);

				// only inform the user if flag is set
				if (data.inform_user)
				{
					VocusFeatures.dialog({header: data.error_title, content: data.error_body, hideAlert: true});
				}
			}

			PubsubClient.call("VocusDaemon", "sync", {});

		});
	},

	getWrappedAnchors: function(html)
	{
		var anchors = [];
		var payload = $("<div>" + html + "</div>");

		payload.find("a").each(function(index, el) {
			var obj = $(el);
			var href = obj.attr("href");
			var name = obj.text();
			var uuid = VocusTracking.getUUID();

			if (href == null || href == "" || name == "") return;
			if (name.startsWith("http")) return; // avoid wrapping naked links
			if (href.startsWith("mailto") || href.startsWith("tel")) return;
			if (!href.startsWith("http")) href = "http://" + href;

			anchors.push({ href: href, name: name, uuid: uuid });

			var newHref = VocusFeatures.settings.trackingEndpoint + "/link?id=" + uuid + "&url=" + encodeURIComponent(href);
			obj.attr("href", newHref);
		});

		return {html: payload.html(), anchors: anchors};
	},

	//
	// Helpers

	getComposeSettings: function(view)
	{
		var box = $(view.getBodyElement()).parents(".M9");
		
		var settings = {
			account: VocusFeatures.iSDK.User.getEmailAddress(),
			enabled: VocusCompose.getProperty(box, "vocusSwitch") != "disabled",
			subject: view.getSubject(),
			followup: VocusCompose.getProperty(box, "vocusFollowup"),
			sendlater: VocusCompose.getProperty(box, "vocusSendlater"),
			snooze: VocusCompose.getProperty(box, "vocusSnooze"),
			tracking: VocusCompose.getProperty(box, "vocusTracking"),
			isDirty: VocusCompose.getProperty(box, "vocusIsDirty") == "true",
			isReply: view.isReply()
		};

		// add threadId
		try {
			settings.gThreadId = box.find("input[name=rt]").val();
		} catch(e) {
			settings.gThreadId = "error: " + e.toString();
		}

		return settings;
	},

	getUUID: function()
	{
		// Taken from https://stackoverflow.com/a/2117523/2721
		return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
			(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
		)
	}
}