var VocusUndo = {
	init: function()
	{
		$(document).on("click", "span[id='vocus-send-undo']", VocusUndo.recall);
	},

	recall: function()
	{
		VocusAlert.alert({content: "Recalling..."});

		var jobId = $(this).attr("data-job-id");
		var params = {
			job: jobId,
			account: VocusFeatures.iSDK.User.getEmailAddress()
		}

		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/recall", params], VocusUndo.recallResult);
	},

	recallResult: function(result)
	{
		VocusAlert.alert({hide: true});

		if (!result.success)
		{
			VocusAlert.alert({content: "Sorry, looks like your message was already processed."});
			return;
		}

		VocusAlert.alert({content: "Recall success, saved to your Drafts folder.", seconds: 7});

		if (result.messageId)
		{
			// VocusCompose.openDraft(result.messageId);
			VocusFeatures.openLastDraft();
		}
	}
}