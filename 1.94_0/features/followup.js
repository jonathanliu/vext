var VocusFollowup =
{
	init: function()
	{
		PubsubClient.subscribe("apply-followup", VocusFollowup.apply);
		PubsubClient.subscribe("compose-property-changed", VocusFollowup.checkProperty);
		PubsubClient.subscribe("snippets-form-submit", VocusFollowup.executeSnippet);
	},

	openPanel: function(hash)
	{
		VocusCPanel.hide();
		VocusCompletion.giveFocusBack();
		followupId = VocusCompose.getProperty(null, "vocusFollowup");

		// update followup
		if (followupId != "")
		{
			var url = "/followups/update?context=" + VocusFeatures.context + "&id=" + followupId + "&close=true";
			VocusDashboard.open(url);
		}

		// new followup
		else
		{
			var url = "/followups/create?compact=true&context=" + VocusFeatures.context;

			if (typeof hash == "string")
				url = url + "#" + hash;

			VocusPopup.open({
				width: (VOCUS_DEMO == true ? 500 : 600),
				height: 450,
				borderRadius: "0px",
				url: url
			});
		}
	},

	apply: function(options)
	{
		// validate
		if (!options.context || options.context != VocusFeatures.context)
			return;

		VocusCompose.setProperty(null, "vocusFollowup", options.followupId);
	},

	remove: function()
	{
		VocusCompose.setProperty(null, "vocusFollowup", "");
	},

	checkProperty: function(changes)
	{
		if (changes.propName != "vocusFollowup")
				return;
			
		if (changes.context && changes.context != VocusFeatures.context)
			return;

		if (changes.propValue == "")
		{
			VocusCompose.removeNote({name: "followup"});
		}
		else
		{
			VocusCompose.addNote({
				name: "followup", 
				body: "A followup sequence will be attached to this message",
				linkLabel: "remove",
				linkHandler: VocusFollowup.remove
			});
		}
	},

	executeSnippet: function(data)
	{
		// do nothing if no followup is attached to this snippet
		if (data.snippet.followupTemplate == null || data.snippet.followupTemplate == "")
			return;

		// do nothing if a followup is already attached to this email
		if (VocusCompose.getProperty(null, "vocusFollowup") != "")
			return;

		// encode data
		var hash = btoa(unescape(encodeURIComponent(JSON.stringify({
			templateId: data.snippet.followupTemplate,
			variables: data.variables
		}))));
		
		VocusFollowup.openPanel(hash);
	}
}