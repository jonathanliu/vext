var VocusTBPanel = 
{
	activeBtn: null,

	init: function()
	{
		// arrange
		VocusTBPanel.dialog = $("#vocus-tbpanel");
		VocusTBPanel.items = $("#vocus-tbpanel-items");
		VocusTBPanel.content = $("#vocus-tbpanel-content");
		VocusTBPanel.msg = $("#vocus-tbpanel-msg");
		VocusTBPanel.loader = $("#vocus-tbpanel-loader");
		
		// create button
		VocusDashboard.button = VocusFeatures.iSDK.Toolbars.registerThreadButton({
			title: "",
			iconUrl: VOCUS_LOGO_PATH,
			onClick: VocusTBPanel.click,
			threadSection: "METADATA_STATE",
			positions: ["THREAD"]
		});

		// observe events
		$(document).mouseup(VocusTBPanel.clickedOutside);
		$("#vocus-tbpanel-snooze").on("click", VocusSnooze.openToolbar);
		$("#vocus-tbpanel-crm").on("click", VocusTBPanel.clickCrmButton);
		$("#vocus-tbpanel-dashboard").on("click", VocusTBPanel.clickOpenDashboard);
	},

	click: function()
	{
		VocusTBPanel.activeBtn = $(document.activeElement);

		if (VocusTBPanel.dialog.is(":visible"))
		{
			VocusTBPanel.hide();
		}
		else
		{
			VocusTBPanel.open();
		}
	},

	clickedOutside: function(e)
	{
		if (!VocusTBPanel.dialog.is(":visible"))
			return;

		// do nothing if clicked somewhere within panel dialog
		if (VocusTBPanel.dialog.is(e.target) || VocusTBPanel.dialog.has(e.target).length > 0)
			return;

		VocusTBPanel.hide();
	},

	clickOpenDashboard: function()
	{
		VocusTBPanel.hide();
		VocusDashboard.open();
	},

	open: function()
	{
		var btn = VocusTBPanel.activeBtn;
		
		// position dialog
		var offset = btn.offset();
		VocusTBPanel.dialog.css("top", offset.top + btn.outerHeight() + 8);
		VocusTBPanel.dialog.css("left", offset.left - VocusTBPanel.dialog.outerWidth() / 2 + btn.outerWidth() / 2);
		VocusTBPanel.dialog.show();
		VocusTBPanel.content.hide();

		// disabled?
		if (VocusFeatures.profile.disabled)
		{
			var msg = VocusFeatures.profile.disabledMessage || "Please sign in to enable Vocus.io";
			
			VocusTBPanel.loader.hide();
			VocusTBPanel.items.hide();
			VocusTBPanel.msg.show().html(msg);
		}

		// enabled?
		else
		{
			VocusTBPanel.resetLoader();
			VocusTBPanel.loader.show();

			VocusTBPanel.prepareCrmButton();
			VocusTBPanel.items.show();

			VocusTBPanel.msg.hide();

			VocusTBPanel.getSummary();
		}
	},

	hide: function()
	{
		VocusTBPanel.dialog.hide();
	},

	resetLoader: function()
	{
		VocusTBPanel.loader.find("img").addClass("fa-spin");
		VocusTBPanel.loader.find("div").html("");
	},

	//
	// Panel message summary 

	getSummary: function()
	{
		var thread = VocusFeatures.activeThreadId;
		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/dashboard/message_summary", {thread: thread}], VocusTBPanel.processSummary);
	},

	processSummary: function(result)
	{
		VocusTBPanel.loader.find("img").removeClass("fa-spin");

		if (result == "")
		{
			VocusTBPanel.loader.find("div").html("no signals received.");
		}
		else
		{
			VocusTBPanel.content.html(result);
			VocusTBPanel.loader.hide();
			VocusTBPanel.content.show();
			$("#vocus-msg-details-btn").on("click", VocusTBPanel.showDetails);
		}
	},

	showDetails: function()
	{
		VocusTBPanel.hide();
		var id = $("#vocus-msg-details-btn").attr("data-message-id");
		VocusDashboard.open("/dashboard/message_details/" + id);
	},

	//
	// Panel log crm

	prepareCrmButton: function()
	{
		if (VocusFeatures.settings.crmName == null)
		{
			$("#vocus-tbpanel-crm").hide();
		}
		else
		{
			$("#vocus-tbpanel-crm span").html("Log to " + VocusFeatures.settings.crmName);
			$("#vocus-tbpanel-crm").show();
		}
	},

	clickCrmButton: function()
	{
		var name = VocusFeatures.settings.crmName;
		var account = VocusFeatures.iSDK.User.getEmailAddress();
		var thread = VocusFeatures.activeThreadId;
		var convView = VocusFeatures.isConversationView();
		var params = {account: account, thread: thread, convView: convView};

		VocusTBPanel.hide();
		VocusAlert.alert({content: "Adding to " + name + "..."});

		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/tracker/log_to_crm", params], VocusTBPanel.processCrmLog);
	},

	processCrmLog: function(result)
	{
		if (!result.success)
		{
			VocusAlert.alert({content: "Something went wrong - failed to log email. Try again or contact support if the problem persists.", seconds: 6});
		}
		else
		{
			var name = VocusFeatures.settings.crmName;
			VocusAlert.alert({content: "Adding to " + name + "... Done!", seconds: 2});
		}
	}
}