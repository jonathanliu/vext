var VocusSnooze = 
{
	// indicator whether we're inside a compose box (true) or a message read (false)
	isComposeView: false,

	init: function()
	{
		// arrange
		VocusSnooze.dialog = $("#vocus-snooze");

		// observe events
		PubsubClient.subscribe("hash-changed", VocusSnooze.hashChanged);
		PubsubClient.subscribe("compose-property-changed", VocusSnooze.checkProperty);
		PubsubClient.subscribe("snippets-expand", VocusSnooze.expandSnippet);

		$(document).mouseup(VocusSnooze.clickedOutside);
	},

	bindObjects: function()
	{
		VocusSnooze.input = $("#vocus-snooze-input");
		VocusSnooze.label = $("#vocus-snooze-label");
		VocusSnooze.create = $("#vocus-snooze-create-btn");
		VocusSnooze.destroy = $("#vocus-snooze-destroy-btn");
		VocusSnooze.responseCB = $("#vocus-snooze-check-response");
		VocusSnooze.noteCb = $("#vocus-snooze-note-checkbox");
		VocusSnooze.noteInput = $("#vocus-snooze-note");

		$("#vocus-snooze-shortcuts span").on("click", VocusSnooze.applyShortcut);
		VocusSnooze.input.on("keyup", VocusSnooze.inputChanged);
		VocusSnooze.noteCb.on("click", VocusSnooze.noteCbClicked);
		VocusSnooze.create.on("click", VocusSnooze.processCreate);
		VocusSnooze.destroy.on("click", VocusSnooze.processDestroy);
	},

	//
	// Show/hide functions

	openPanel: function()
	{
		VocusSnooze.isComposeView = true;

		var dom = $("#vocus-snooze-template").html().trim();
		VocusCPanel.setContent(dom);
		VocusSnooze.bindObjects();

		VocusSnooze.destroy.html("Unsnooze");
		VocusSnooze.applyOptions();
	},

	openToolbar: function()
	{
		VocusSnooze.isComposeView = false;

		var btn = VocusTBPanel.activeBtn;
		VocusTBPanel.hide();

		// set dom
		var dom = $("#vocus-snooze-template").html().trim();
		$("#vocus-snooze-content").html(dom);
		VocusSnooze.bindObjects();

		// position dialog
		var offset = btn.offset();
		VocusSnooze.dialog.css("top", offset.top + btn.outerHeight() + 8);
		VocusSnooze.dialog.css("left", offset.left - VocusSnooze.dialog.outerWidth() / 2 + btn.outerWidth() / 2);
		VocusSnooze.dialog.show();

		$("#vocus-snooze-content").show();
		$("#vocus-snooze-msg").hide();
		VocusSnooze.destroy.html("Unsnooze");
		VocusSnooze.applyOptions();
	},

	closeToolbar: function()
	{
		VocusSnooze.dialog.hide();
		$("#vocus-snooze-content").html("");
	},

	clickedOutside: function(e)
	{
		if (!VocusSnooze.dialog.is(":visible"))
			return;

		// do nothing if clicked somewhere within panel dialog
		if (VocusSnooze.dialog.is(e.target) || VocusSnooze.dialog.has(e.target).length > 0)
			return;

		VocusSnooze.closeToolbar();
	},

	//
	// Checkbox/input interactions

	noteCbClicked: function()
	{
		VocusSnooze.noteInput.toggleClass("visible");

		if (!VocusSnooze.noteInput.hasClass("visible"))
		{
			VocusSnooze.noteInput.val("");
		}

		if (VocusSnooze.isComposeView)
		{
			VocusCPanel.repositionDialog();
		}
	},

	applyOptions: function()
	{
		// defaults
		var options = {
			time: "",
			checkResponse: true,
			note: ""
		};

		// update old thread
		if (VocusSnooze.isInsideSnooze())
		{
			// buttons
			VocusSnooze.create.html("Update");
			VocusSnooze.destroy.show();

			// change options
			var snooze = VocusSnooze.getCurrentSnooze();
			
			if (typeof snooze != "undefined")
			{
				if (snooze.time != "") options.time = Date.create(snooze.time).long();
				options.checkResponse = snooze.checkResponse;
				options.note = snooze.note;
			}
		}

		// create new thread
		else
		{
			// buttons
			VocusSnooze.create.html("Done");
			VocusSnooze.destroy.hide();
		}

		// if compose view, see if there's existing values
		if (VocusSnooze.isComposeView)
		{
			var prop = VocusCompose.getProperty(null, "vocusSnooze");

			if (prop == "")
				return;
			
			prop = JSON.parse(atob(prop));
			options.time = prop.time;
			options.checkResponse = prop.checkResponse;
			options.note = prop.note;
		}

		// apply time option
		VocusSnooze.input.val(options.time);
		VocusSnooze.inputChanged();

		// apply note
		if (options.note != null && options.note.trim() != "")
		{
			VocusSnooze.noteInput.addClass("visible");
			VocusSnooze.noteInput.val(options.note);
			VocusSnooze.noteCb.attr("checked", "checked");

			if (VocusSnooze.isComposeView)
				VocusCPanel.repositionDialog();
		}
		else
		{
			VocusSnooze.noteInput.removeClass("visible");
			VocusSnooze.noteInput.val("");
			VocusSnooze.noteCb.removeAttr("checked");
		}

		// apply response checkbox
		VocusSnooze.responseCB.attr("checked", options.checkResponse ? "checked" : null);
	},

	applyShortcut: function()
	{
		var shortcut = $(this).html();
		var time = "";

		if (shortcut.indexOf("1") > -1) time += "One ";
		if (shortcut.indexOf("2") > -1) time += "Two ";
		if (shortcut.indexOf("3") > -1) time += "Three ";
		if (shortcut.indexOf("8") > -1) time += "Eight ";
		if (shortcut.indexOf("H") > -1) time += "hour";
		if (shortcut.indexOf("D") > -1) time += "day";
		if (shortcut.indexOf("W") > -1) time += "week";
		if (shortcut.indexOf("1") == -1) time += "s";

		VocusSnooze.input.val(time + " later");
		VocusSnooze.inputChanged();
	},

	inputChanged: function()
	{
		// translate human to actual
		var human = VocusSnooze.input.val().toLowerCase().trim();

		// now or disabled
		if (human == "" || human == "now")
		{
			VocusSnooze.setLabel(null);
		}

		// later
		else
		{
			var parsed = Date.future(human).long();
			VocusSnooze.setLabel(parsed);
		}
	},

	setLabel: function(value)
	{
		if (!value)
		{
			VocusSnooze.label.html("Immediate");
			VocusSnooze.create.removeClass("enabled");
		}
		else if (value == "Invalid Date")
		{
			VocusSnooze.label.html("Invalid Date");
			VocusSnooze.create.removeClass("enabled");
		}
		else
		{
			VocusSnooze.label.html(value);
			VocusSnooze.create.addClass("enabled");
		}
	},

	//
	// Buttons: Snooze, Unsnooze

	processCreate: function()
	{
		if (!VocusSnooze.create.hasClass("enabled"))
			return;

		// inside compose?
		if (VocusSnooze.isComposeView)
		{
			VocusSnooze.processCreateCompose();
		}

		// inside email?
		else
		{
			VocusSnooze.processCreateInbox();
		}
	},

	processCreateCompose: function(params)
	{
		console.log("Adding property to snooze...");

		if (params == null)
		{
			params = {
				time: Date.create(VocusSnooze.label.html()).toISOString(),
				checkResponse: VocusSnooze.responseCB.is(":checked"),
				note: VocusSnooze.noteInput.val()
			};
		}

		var value = btoa(JSON.stringify(params));
		VocusCompose.setProperty(null, "vocusSnooze", value);

		VocusCPanel.hide();
	},

	processCreateInbox: function()
	{
		// show loading
		VocusSnooze.create.removeClass("enabled");
		VocusSnooze.create.html("<i class='fa fa-circle-o-notch fa-spin'></i>");

		// arrange
		var params = {
			account: VocusFeatures.iSDK.User.getEmailAddress(),
			time: Date.create(VocusSnooze.label.html()).toISOString(), 
			threadId: VocusFeatures.activeThreadId,
			subject: VocusFeatures.activeThreadSubject,
			checkResponse: VocusSnooze.responseCB.is(":checked"),
			note: VocusSnooze.noteInput.val(),
			convView: VocusFeatures.isConversationView()
		};

		// submit
		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/snooze/create", params], function(result)
		{
			if (result.success)
			{
				VocusFeatures.refresh();
				VocusSnooze.closeToolbar();
				PubsubClient.call("VocusDaemon", "sync");
				VocusFeatures.alert({content:"Message snoozed, moved to your Snooze folder.", seconds: 7});
			}
			else if (result.success == false && typeof result.message != "undefined")
			{
				VocusAlert.dialog({header: "Error processing snooze", content: result.message})
			}
			else
			{
				VocusFeatures.alert({content:"Something went wrong, can't snooze your message.", seconds: 7});
			}

			VocusSnooze.create.addClass("enabled");
			VocusSnooze.create.html("Done");
		});
	},

	processDestroy: function()
	{
		// show loading
		VocusSnooze.destroy.html("<i class='fa fa-circle-o-notch fa-spin'></i>");

		var params = {
			account: VocusFeatures.iSDK.User.getEmailAddress(),
			threadId: VocusFeatures.activeThreadId,
			convView: VocusFeatures.isConversationView()
		};

		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/snooze/destroy", params], function(result)
		{
			if (result.success)
			{
				PubsubClient.call("VocusDaemon", "sync");
				VocusFeatures.alert({content:"Message unsnoozed, moved to your inbox.", seconds: 7});
			}
			else
			{
				VocusFeatures.alert({content:"Something went wrong, can't unsnooze your message.", seconds: 7});
			}

			VocusFeatures.refresh();
			VocusSnooze.closeToolbar();
		});
	},

	//
	// Helpers

	isInsideSnooze: function()
	{
		if (window.location.hash.indexOf("label/Snooze/") == -1)
			return false;

		return true;
	},

	getCurrentSnooze: function()
	{
		var thread = VocusFeatures.activeThreadId;

		// Gmail's "Conversation View" turned On (Default)
		var snooze = underscore.find(VocusFeatures.profile.snoozes, function(i) { return i.thread_id == thread; });
		if (snooze) return snooze;

		// Gmail's "Conversation View" turned Off
		var snooze = underscore.find(VocusFeatures.profile.snoozes, function(i) { return i.email_id == thread; });
		if (snooze) return snooze;

		return false;
	},

	hashChanged: function()
	{
		VocusSnooze.closeToolbar();
	},

	//
	// Compose property functions

	removeFromCompose: function()
	{
		VocusCompose.setProperty(null, "vocusSnooze", "");
	},

	checkProperty: function(changes)
	{
		if (changes.propName != "vocusSnooze")
			return;
		
		if (changes.context && changes.context != VocusFeatures.context)
			return;

		if (changes.propValue == "")
		{
			VocusCompose.removeNote({name: "snooze"});
		}
		else
		{
			VocusCompose.addNote({
				name: "snooze", 
				body: "A reminder will be attached to this message",
				linkLabel: "remove",
				linkHandler: VocusSnooze.removeFromCompose
			});
		}
	},

	//
	// Snooze via snippet

	expandSnippet: function(snippet)
	{
		if (snippet.snoozeTime == null || snippet.snoozeTime == "")
			return;
		
		var humanTime = snippet.snoozeTime.toLowerCase().trim();
		var actualTime = Date.future(humanTime);

		if (Date.create() >= actualTime)
		{
			alert("Failed to attach Reminder: the time value '" + humanTime + "' is not relative, already passed.");
			return;
		}

		VocusSnooze.processCreateCompose({
			time: actualTime.toISOString(),
			checkResponse: snippet.snoozeCheckResponse,
			note: ""
		});
	}
}