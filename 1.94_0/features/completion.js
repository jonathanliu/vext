var VocusCompletion = 
{
	isShowing: false,
	commands: [],

	init: function()
	{
		// arrange
		VocusCompletion.box = $("#vocus-completion");

		// inject completion
		VocusFeatures.iSDK.Compose.registerComposeViewHandler(function(composeView) {
			VocusCompletion.injectCompletion($(composeView.getBodyElement()));
		});

		// observe hiding events
		PubsubClient.subscribe("compose-state-changed", VocusCompletion.uninjectCompletion);
	},

	addCommand: function(owner, name, callback)
	{
		VocusCompletion.commands.push({
			"owner": owner,
			"name": name,
			"callback": callback
		});
	},

	resetCommands: function(owner)
	{
		VocusCompletion.commands = underscore.filter(VocusCompletion.commands, function(item) {
			return item.owner != owner;
		});
	},

	getExpansion: function(name)
	{
		var cmd = underscore.find(VocusCompletion.commands, function(item) {
			return item.name == name;
		});

		var result = cmd.callback(name);

		if (result)
			return "<div>" + result + "</div>";
		else
			return null;
	},

	injectCompletion: function(box)
	{
		if (VocusFeatures.profile.disabled)
			return;

		box.attr("data-vocus-completion", "true");

		box.on("keydown", VocusCompletion.keydown);
		box.on("keyup", VocusCompletion.keyup);
		box.on("mouseup", VocusCompletion.clickEditor);

		// // HAACK: observe trimmed btn in message body
		setTimeout(function() {
			var trimBtn = $(".M9 div.ajR");
			
			trimBtn.on("focus", function() {
				VocusCompletion.giveFocusBack();
			});
		}, 2000);
	},

	uninjectCompletion: function(state)
	{
		if (VocusCompletion.isShowing)
			VocusCompletion.hide();
	},

	giveFocusBack: function()
	{
		$("div.Am.Al.editable").focus();
	},

	remove: function(e)
	{
		VocusCompletion.hide();
	},

	clickEditor: function()
	{
		if (!VocusCompletion.isShowing)
			return;

		VocusCompletion.hide();
	},

	keyup: function(e)
	{
		if (!VocusCompletion.isShowing)
			return;

		if (e.which == 38 || e.which == 40 || e.shiftKey) // arrow keys up/down and shift
			return;

		if (e.key == VocusFeatures.profile.completionChar)
		{
			VocusCompletion.lastToken = VocusFeatures.profile.completionChar;
			return;
		}

		if (e.which == 32) // escape
		{
			VocusCompletion.hide();
			return;
		}

		// arrange
		var range = getSelection().getRangeAt(0).cloneRange();
		var content = range.startContainer.textContent;
		var pos = range.startOffset;

		// get current token
		if (range.startContainer.length == 1)
		{
			var token = content;
		}
		else
		{
			var start = pos;
			var end = pos;
			while(start - 1 >= 0) { if (content[start-1] == " ") break; start--; }
			while(end < content.length) { if (content[end] == " ") break; end++; }
			var token = content.substr(start, end - start);
			VocusCompletion.lastToken = token;
		}
		
		// hide if token is not command
		if (!token.startsWith(VocusFeatures.profile.completionChar))
		{
			VocusCompletion.hide();
			return;
		}

		// get command from token and filter
		var cmd = token.substr(1);
		VocusCompletion.filter(cmd);
	},

	keydown: function(e)
	{
		// don't show if not authenticated
		if (!VocusFeatures.auth)
			return;

		// show
		if (e.key == VocusFeatures.profile.completionChar && !VocusCompletion.isShowing) // slash
		{
			VocusCompletion.lastToken = VocusFeatures.profile.completionChar;
			VocusCompletion.show();
			return;
		}

		if (!VocusCompletion.isShowing)
			return;

		// control selection
		if (e.which == 38) // up
		{
			VocusCompletion.goUp();
			VocusCompletion.scrollToCommand();
			e.preventDefault();
			return false;
		}
		else if (e.which == 40) // down
		{
			VocusCompletion.goDown();
			VocusCompletion.scrollToCommand();
			e.preventDefault();
			return false;
		}
		else if (e.which == 13) // enter
		{
			VocusCompletion.open();
			e.preventDefault();
			return false;
		}
	},

	show: function()
	{
		// get coords
		var range = getSelection().getRangeAt(0).cloneRange();
		var elem = $("<span>\u200b</span>");
		range.insertNode(elem[0]);
		rects = elem[0].getClientRects()[0];
		elem.remove();

		// set coords
		VocusCompletion.box.css("top", rects.bottom + "px");
		VocusCompletion.box.css("left", rects.left + "px");

		// show it
		VocusCompletion.isShowing = true;
		VocusCompletion.box.show();
		$("#vocus-commands").scrollTop(0);

		// don't filter
		VocusCompletion.filter("");
	},

	filter: function(name)
	{
		// clear commands list
		$("#vocus-commands").html("");

		// populate commands
		for(var i = 0; i < VocusCompletion.commands.length; i++)
		{
			var cmd = VocusCompletion.commands[i];

			if (!cmd.name.includes(name))
				continue;

			var button = $("<div class='command'></div>");
			var newLabel = cmd.name.replace(name, "<span>" + name + "</span>");
			button.html(newLabel);
			button.on("mouseover", VocusCompletion.hover);
			button.on("click", VocusCompletion.click);

			$("#vocus-commands").append(button);
		}

		var commands = $("#vocus-commands .command");

		// hide if empty
		if (commands.length == 0)
		{
			VocusCompletion.hide();
		}

		// select the first one
		else
		{
			commands.first().addClass("selected");
		}
	},

	hover: function()
	{
		var current = VocusCompletion.box.find(".selected");
		current.removeClass("selected");
		$(this).addClass("selected");
	},

	click: function(e)
	{
		VocusCompletion.giveFocusBack();
		VocusCompletion.open();
		VocusCompletion.hide();
	},

	hide: function()
	{
		VocusCompletion.isShowing = false;
		VocusCompletion.box.hide();
	},

	goUp: function()
	{
		var current = VocusCompletion.box.find(".selected");

		if (current.is(":first-child"))
			return;

		current.removeClass("selected");
		current.prev().addClass("selected");
	},

	goDown: function()
	{
		var current = VocusCompletion.box.find(".selected");

		if (current.is(":last-child"))
			return;

		current.removeClass("selected");
		current.next().addClass("selected");
	},

	scrollToCommand: function()
	{
		var commands = $("#vocus-commands");
		var itemHeight = commands.find(".command").first().outerHeight();

		var index = VocusCompletion.box.find(".selected").index();
		var visibleStart = commands.scrollTop();
		var visibleEnd = visibleStart + commands.height();
		var itemStart = index*itemHeight;
		var itemEnd = (index+1)*itemHeight;

		if (itemStart < visibleStart)
			commands.scrollTop(itemStart);
		else if (itemEnd > visibleEnd)
			commands.scrollTop(itemEnd - commands.height());
	},

	open: function()
	{
		var name = VocusCompletion.box.find(".selected").text();
		VocusCompletion.hide();

		// arrange
		var range = getSelection().getRangeAt(0);
		var clone = range.cloneRange();

		// remove token
		clone.setStart(range.startContainer, range.startOffset - VocusCompletion.lastToken.length);
		clone.setEnd(range.startContainer, range.startOffset);
		clone.deleteContents();

		// get expansion
		var expansion = VocusCompletion.getExpansion(name);
		
		// insert and move to the end
		if (expansion)
		{
			var node = $(expansion);
			clone.insertNode(node[0]);

			// move caret to end
			range = document.createRange();
			var sel = window.getSelection();
			range.setStart(node[0], 2);
			range.collapse(true);
			sel.removeAllRanges();
			sel.addRange(range);
		}
	}
};