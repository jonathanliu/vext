var VocusMeeting =
{
	init: function()
	{
		VocusCompletion.resetCommands("VocusMeeting");
		VocusCompletion.addCommand("VocusMeeting", "meeting", VocusMeeting.open);
	},

	open: function()
	{
		VocusPopup.open({
			width: (VOCUS_DEMO ? 500 : 900),
			height: 500,
			url: "/meetings/create?context=" + VocusFeatures.context + "&account=" + VocusFeatures.iSDK.User.getEmailAddress()
		});
	},

	openPanel: function()
	{
		VocusCPanel.hide();
		VocusCompletion.giveFocusBack();
		VocusMeeting.open();
	}
};