var VocusPoll =
{
	init: function()
	{
		VocusCompletion.resetCommands("VocusPoll");
		VocusCompletion.addCommand("VocusPoll", "poll", VocusPoll.open);
	},

	open: function()
	{
		VocusPopup.open({
			width: 500,
			height: 320,
			url: "/polls/create?context=" + VocusFeatures.context
		})
	},

	openPanel: function()
	{
		VocusCPanel.hide();
		VocusCompletion.giveFocusBack();
		VocusPoll.open();
	}
};