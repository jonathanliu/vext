var VocusAlert =
{
	init: function()
	{

	},

	alert: function(data)
	{
		if (data.hide)
		{
			VocusFeatures.iSDK.ButterBar.hideMessage("vocus-msg");
		}
		else
		{
			VocusFeatures.iSDK.ButterBar.hideMessage("vocus-msg");

			if (!data.seconds)
			{
				data.seconds = 10000;
			}

			VocusFeatures.iSDK.ButterBar.showMessage({
				messageKey: "vocus-msg",
				html: data.content,
				time: data.seconds * 1000
			});
		}
	},

	dialog: function(data)
	{
		var dialog = $("#vocus-native-dialog");
		var bg = $("#vocus-native-dialog-bg");

		// header
		$("#vocus-native-dialog-header span:first").html(data.header);

		// content
		$("#vocus-native-dialog-content").html(data.content);

		// buttons
		if (data.buttons)
		{
			$("#vocus-native-dialog-controls").html("");

			data.buttons.forEach((elem) => {
				var btn = $("<button class='J-at1-auR J-at1-atl'></button>");
				btn.addClass(elem.class);
				btn.html(elem.label);
				btn.on("click", elem.handler);
				$("#vocus-native-dialog-controls").append(btn);
			})
		}
		else
		{
			var btn = $("<button class='J-at1-auR J-at1-atl'>Ok</button>");
			btn.on("click", function() { VocusAlert.dialog({hide: true}) });
			$("#vocus-native-dialog-controls").html(btn);
		}

		// centerize buttons
		if (data.centerButtons)
		{
			$("#vocus-native-dialog-controls").css("text-align", "center");
		}
		else
		{
			$("#vocus-native-dialog-controls").css("text-align", "left");
		}

		// hide alert?
		if (data.hideAlert)
		{
			VocusAlert.alert({hide: true});
		}

		// show them
		if (data.hide)
		{
			$("#vocus-native-dialog-bg").hide();
			$("#vocus-native-dialog").hide();
		}
		else
		{
			// centerize
			dialog.css("top", $(window).height()/2 - dialog.outerHeight() / 2 - 100);
			dialog.css("left", $(window).width()/2 - dialog.outerWidth() / 2);

			// show
			$("#vocus-native-dialog-bg").show();
			$("#vocus-native-dialog").show();
		}
	}
};