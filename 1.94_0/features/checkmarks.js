var VocusCheckmarks =
{
	rowStates: {},
	lastSync: null,
	isUsingLegacyGmailThreadId: false,

	STATE_UNSENT: 0,
	STATE_SENT: 1,
	STATE_OPENED: 2,

	init: function()
	{
		// arrange
		VocusCheckmarks.popup = $("#vocus-checkmarks-popup");
		VocusCheckmarks.content = $("#vocus-checkmarks-popup-content");
		VocusCheckmarks.loader = $("#vocus-checkmarks-popup-loader");

		// listen to page changes
		PubsubClient.subscribe("hash-changed", VocusCheckmarks.configure);
		PubsubClient.subscribe("profile-changed", VocusCheckmarks.onProfileChanged);

		// run it first time in case we are in /sent
		PubsubClient.get("VocusDaemon", "profile", () => {
			VocusCheckmarks.configure()
		}, 1000);

		// listen for hover
		$(document).on("mouseenter", ".vocus-checkmarks", VocusCheckmarks.showPopup);
		$(document).on("mouseleave", "#vocus-checkmarks-popup", VocusCheckmarks.hidePopup);
		$(document).on("click", "#vocus-checkmarks-popup-content #vocus-msg-details-btn", VocusCheckmarks.showDetails);

		// observe new rows to add checkmarks
		DomObserver.attach("body", {childList: true, subtree: true}, VocusCheckmarks.onRowsChanged);
	},

	shouldRun: function()
	{
		if (!VocusFeatures.auth) return false;
		if (!VocusFeatures.settings.defaultVocusEnabled) return false; // default to disable
		if (VocusFeatures.profile.disabled) return false; // account expired (or other reasons)
		if (!VocusFeatures.settings.enableCheckmarks) return false; // user opted for no checkmarks
		if (window.location.hash.indexOf("#sent") != 0) return false; // for now, only show in #sent

		return true;
	},

	configure: function()
	{
		// make sure we are allowed to run
		if (!VocusCheckmarks.shouldRun()) return;

		// add good? enhance UI, then call server
		VocusCheckmarks.addCheckmarkIcons();
		VocusCheckmarks.syncRowStates();
	},

	onProfileChanged: function()
	{
		if (VocusFeatures.settings.enableCheckmarks)
		{
			VocusCheckmarks.configure();
		}
		else
		{
			$(".vocus-checkmarks").remove();
		}
	},

	onRowsChanged: function(mut)
	{
		// make sure we are allowed to run
		if (!VocusCheckmarks.shouldRun()) return;

		mut.addedNodes.forEach(function(elem) {
			if (typeof elem == "undefined" || typeof elem.classList == "undefined") return;
			if (!elem.classList.contains("yX") || !elem.classList.contains("xY")) return;
			if (elem.parentNode.firstElementChild.className == "vocus-checkmarks") return;
			var obj = $(elem);
			obj.parents("tr[data-vocus-enhanced]").removeAttr("data-vocus-enhanced");
		});

		VocusCheckmarks.addCheckmarkIcons();
	},

	//
	// Row Data and UI

	addCheckmarkIcons: function()
	{
		// determine whether to pull new-gmail-thread-id or legacy-gmail-thread-id
		if ($("span[data-legacy-thread-id]").length > 0)
		{
			VocusCheckmarks.isUsingLegacyGmailThreadId = true;
		}
		else
		{
			VocusCheckmarks.isUsingLegacyGmailThreadId = false;
		}

		// process each row
		$("div[role=main] tr:not([data-vocus-enhanced])").each(function() {
			
			// arrange
			var row = $(this);

			// get thread id
			var threadId = null;
			if (VocusCheckmarks.isUsingLegacyGmailThreadId)
			{
				threadId = row.find("span[data-legacy-thread-id]").attr("data-legacy-thread-id");
			}
			else
			{
				threadId = row.find("span[data-thread-id]").attr("data-thread-id");
			}

			// add icon
			row.find("td.yX.xY").prepend("<div class='vocus-checkmarks'>" + VocusCheckmarks.CHECKMARK_SVG + "</div>");

			// mark it as enhanced
			row.attr("data-vocus-enhanced", true);
			row.attr("data-vocus-thread-id", threadId);

			// update the status
			VocusCheckmarks.updateRowState(threadId);
		});
	},

	syncRowStates: function()
	{
		// do nothing if last sync is less than 1min ago, of user is navigating deep pages in #sent, or is in debug mode
		if (VocusCheckmarks.lastSync && window.location.hash.indexOf("#sent/p") == -1 && !VOCUS_IS_DEBUG)
		{
			var diff = new Date() - VocusCheckmarks.lastSync;
			if (diff < 60*1000) { return };
		}

		// get thread IDs
		var threadIds = $("[data-vocus-thread-id]").map(function() {
			var row = $(this);
			return row.attr("data-vocus-thread-id");
		}).toArray();

		// arrange params
		var params = {
			threads: threadIds,
			legacy: VocusCheckmarks.isUsingLegacyGmailThreadId,
			version: VOCUS_VERSION
		};

		// get threads status
		var url = VOCUS_SERVER + "/dashboard/checkmark_state";

		PubsubClient.async("VocusDaemon", "postHTTP", [url, params], function(data) {
			// all good? process
			if (data.success)
			{
				var threads = Object.keys(data.state);
				underscore.each(threads, (threadId) => VocusCheckmarks.setRowState(threadId, data.state[threadId]));
			}

			// something went wrong? write to console
			else
			{
				console.log(`[VOCUS] Error loading checkmark states: ${data.message}`);
			}
		});

		// remember last sync to avoid DDoS
		VocusCheckmarks.lastSync = new Date();

		// TODO:
		// Potentially consider refreshing data if browser receives notification of email open/link click.
	},

	setRowState: function(id, state)
	{
		this.rowStates[id] = state;
		VocusCheckmarks.updateRowState(id);
	},

	updateRowState: function(id)
	{
		var state = VocusCheckmarks.rowStates[id];

		var stroke1 = "#AAA";
		var stroke2 = "#AAA";

		if (state == VocusCheckmarks.STATE_SENT)
		{
			stroke1 = "#AAA";
			stroke2 = "#2ecc71";
		}
		if (state == VocusCheckmarks.STATE_OPENED)
		{
			stroke1 = "#2ecc71";
			stroke2 = "#2ecc71";
		}

		$(`tr[data-vocus-thread-id="${id}"] .vocus-checkmarks .check1`).attr("stroke", stroke1);
		$(`tr[data-vocus-thread-id="${id}"] .vocus-checkmarks .check2`).attr("stroke", stroke2);
	},

	//
	// Popup functions

	showPopup: function()
	{
		var checkmarks = $(this);
		var offset = checkmarks.offset();
		VocusCheckmarks.popup.css("top", offset.top + checkmarks.outerHeight() + 8);
		VocusCheckmarks.popup.css("left", offset.left - VocusCheckmarks.popup.outerWidth() / 2 + checkmarks.outerWidth() / 2);
		VocusCheckmarks.popup.show();
		VocusCheckmarks.content.hide();
		VocusCheckmarks.loader.show();

		var params = {
			thread: checkmarks.parents("[data-vocus-thread-id]").attr("data-vocus-thread-id"),
			legacy: VocusCheckmarks.isUsingLegacyGmailThreadId
		};

		PubsubClient.async("VocusDaemon", "postHTTP", [VOCUS_SERVER + "/dashboard/checkmark_details", params], function(result) {
			VocusCheckmarks.content.html(result);
			VocusCheckmarks.loader.hide();
			VocusCheckmarks.content.show();
		});
	},

	hidePopup: function()
	{
		VocusCheckmarks.popup.hide();
	},

	showDetails: function()
	{
		VocusCheckmarks.hidePopup();
		var id = $(this).attr("data-message-id");
		VocusDashboard.open("/dashboard/message_details/" + id);
	},

	//
	// Helpers

	isPartOfThread: function(row)
	{
		// TODO: ideally we'd check if primary email or alias is within HTML. However we do not have
		// access to alias emails. Will need to get them from backend. Will keep as-is for now and just check for #sent box.
		return true;
	},

	//
	// Checkmark SVG

	CHECKMARK_SVG: `
	<svg width="22px" height="24px" viewBox="0 0 110 100">
	<g stroke="#AAA" stroke-width="8" stroke-linecap="round" stroke-linejoin="round" fill="none" fill-rule="evenodd">
		<path d="M9,63 L29,75 L62,27" class="check1" transform="translate(35, 51) rotate(-355) translate(-35, -51) "></path>
		<path d="M52,67 L66,75 L99,27" class="check2" transform="translate(75, 51) rotate(-355) translate(-75, -51) "></path>
	</g>
	</svg>`
}