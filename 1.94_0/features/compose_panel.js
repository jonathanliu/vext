var VocusCPanel =
{
	activeBtn: null,
	dialog: null,

	ENABLED_LOGO: VOCUS_IMAGES_PATH + "icon.png",
	DISABLED_LOGO: VOCUS_IMAGES_PATH + "icon-disabled.png",

	init: function()
	{
		// populate options
		VocusCPanel.addSection("Configure");
		VocusCPanel.addOption("Tracking", "fa fa-search", VocusTracking.openPanel);
		VocusCPanel.addOption("Reminder", "fa fa-hourglass-o", VocusSnooze.openPanel);
		VocusCPanel.addOption("Followups", "fa fa-bell-o", VocusFollowup.openPanel);
		VocusCPanel.addOption("Scheduling", "fa fa-clock-o", VocusSendlater.openPanel);
		VocusCPanel.addSection("Insert");
		VocusCPanel.addOption("Coordinator", "fa fa-comment-o", VocusMeeting.openPanel);
		VocusCPanel.addOption("Micro-Poll", "fa fa-question", VocusPoll.openPanel);
		VocusCPanel.addOption("Snippet", "fa fa-pencil", VocusSnippets.openPanel);

		// arrange
		VocusCPanel.dialog = $("#vocus-panel");

		// add compose btn
		VocusFeatures.iSDK.Compose.registerComposeViewHandler(function(view) {
			view.addButton({
				title: "",
				iconClass: "vocus-compose-icon",
				iconUrl: VocusCPanel.ENABLED_LOGO,
				onClick: VocusCPanel.open
			});

			var box = $(view.getBodyElement()).parents(".M9");
			VocusCPanel.refreshIcon(box);
		});

		// observe events
		PubsubClient.subscribe("compose-hidden", VocusCPanel.hide);
		$(document).mouseup(VocusCPanel.clickedOutside);

		// prepare switch
		$("#vocus-switch-bar").click(VocusCPanel.switchClicked);
	},

	clickedOutside: function(e)
	{
		if (!VocusCPanel.dialog.is(":visible"))
			return;

		// do nothing if clicked on panel button
		if ($(e.target).parents("#vocus-panel-button").length > 0)
			return;

		// do nothing if clicked somewhere within panel dialog
		if (VocusCPanel.dialog.is(e.target) || VocusCPanel.dialog.has(e.target).length > 0)
			return;

		VocusCPanel.hide();
	},

	open: function()
	{
		var email = VocusFeatures.getEmailAddress();

		// set current state
		var state = VocusCompose.getProperty(null, "vocusSwitch");
		var stateFlag = (state == "enabled");
		VocusCPanel.setVocusSwitchState(null, stateFlag);

		// show internal UI
		$("#vocus-panel-items").show();
		$("#vocus-panel-content").hide();

		// reposition and show dialog
		VocusCPanel.activeBtn = $(document.activeElement);
		VocusCPanel.repositionDialog();
		VocusCPanel.dialog.show();

		// show disabled message
		if (VocusFeatures.profile.disabled)
		{
			var content = $("<div id='vocus-panel-disabled-message'></div>");
			var msg = VocusFeatures.profile.disabledMessage || "Please sign in to enable Vocus.io by clicking on the Vocus.io button on the top right of your inbox.";
			content.html(msg);
			VocusCPanel.setContent(content);

			$("#vocus-panel-content").removeClass("disabled-content");
			$("#vocus-switch-indicator").hide();
		}

		// show 'account not configured' message
		else if (!VocusFeatures.isAccount(email))
		{
			var content = $("<div id='vocus-panel-disabled-message'></div>");
			var msg = `Vocus.io was not activated on this inbox. To activate it, click <a target='_blank' href='https://vocus.io/add_inbox'>here</a> and sign in as <span style='word-break: break-all'>(${email})</span>.`;
			content.html(msg);
			VocusCPanel.setContent(content);

			$("#vocus-panel-content").removeClass("disabled-content");
			$("#vocus-switch-indicator").hide();
		}

		// all good? show the goods
		else
		{
			$("#vocus-switch-indicator").show();
		}
	},

	hide: function()
	{
		$("#vocus-panel-content").html("");
		VocusCPanel.dialog.hide();
	},

	repositionDialog: function()
	{
		var btn = VocusCPanel.activeBtn;
		var offset = btn.offset();

		VocusCPanel.dialog.css("top", offset.top - VocusCPanel.dialog.outerHeight() - 10);
		VocusCPanel.dialog.css("left", offset.left - VocusCPanel.dialog.outerWidth() / 2 + btn.outerWidth() / 2);
	},

	addOption: function(name, icon, delegate)
	{
		var option = $("<div class='vocus-panel-item'><i class='" + icon + "'></i>" + name + "</div>");
		option.on("click", delegate);
		$("#vocus-panel-items").append(option);
	},

	addSection: function(name)
	{
		var option = $("<div class='vocus-panel-section'>" + name + "</div>");
		$("#vocus-panel-items").append(option);
	},

	setContent: function(content, isHtml)
	{
		if (isHtml)
		{
			$("#vocus-panel-content").html("");
			$("#vocus-panel-content").append(content);
		}
		else
		{
			$("#vocus-panel-content").html(content);
		}

		$("#vocus-panel-items").hide();
		$("#vocus-panel-content").show();
		VocusCPanel.repositionDialog();
	},

	setVocusSwitchState: function(box, enabled)
	{
		if (box == null)
			box = VocusCompose.getCurrentCompose();

		if (!enabled)
		{
			// include note and property
			$("#vocus-switch-bar").removeClass("enabled").addClass("disabled");
			$("#vocus-switch-title").html("Vocus.io disabled");
			VocusCompose.setProperty(box, "vocusSwitch", "disabled");
			
			// disable content
			$("#vocus-panel-items").addClass("disabled-content");
			$("#vocus-panel-content").addClass("disabled-content");
		}
		else
		{
			// remove note and property
			$("#vocus-switch-bar").removeClass("disabled").addClass("enabled");
			$("#vocus-switch-title").html("Vocus.io enabled");
			VocusCompose.setProperty(box, "vocusSwitch", "enabled");
			VocusCompose.removeNote({name: "switch"});

			// enable content
			$("#vocus-panel-items").removeClass("disabled-content");
			$("#vocus-panel-content").removeClass("disabled-content");
		}

		VocusCPanel.refreshIcon(box);
	},

	getVocusSwitchState: function()
	{
		return $("#vocus-switch-bar").is(".enabled");
	},

	refreshIcon: function(box)
	{
		var state = VocusCPanel.getVocusSwitchState();
		var img = box.find(".vocus-compose-icon img");

		if (img.length == 0)
			return;

		if (!state)
		{
			img.attr("src", VocusCPanel.DISABLED_LOGO);
			
		}
		else
		{
			img.attr("src", VocusCPanel.ENABLED_LOGO);
		}
	},

	switchClicked: function()
	{
		var state = VocusCPanel.getVocusSwitchState();
		VocusCPanel.setVocusSwitchState(null, !state);
	}
}