// load scripts
bootstrapVocusCss("css/font-awesome.css");
bootstrapVocusCss("css/app.css");
bootstrapVocusScript("core/constants.js");
bootstrapVocusContent("html/content.html");

// set vars
VOCUS_IMAGES_PATH = "chrome-extension://" + chrome.runtime.id + "/images/"; 
VOCUS_LOGO_PATH = VOCUS_IMAGES_PATH + "icon.png";
VOCUS_EXT_ID = chrome.runtime.id;
VOCUS_VERSION = chrome.runtime.getManifest()["version"];

bootstrapVocusVar("VOCUS_EXT_ID", VOCUS_EXT_ID);
bootstrapVocusVar("VOCUS_IMAGES_PATH", VOCUS_IMAGES_PATH);
bootstrapVocusVar("VOCUS_LOGO_PATH", VOCUS_LOGO_PATH);
bootstrapVocusVar("VOCUS_VERSION", VOCUS_VERSION);

function bootstrapVocusContent(name)
{
	console.log("[Vocus] Loading " + name);
	
	var url = chrome.extension.getURL(name);

	$.get(url, (data) => {
		$("body").prepend(data.replace(/VOCUS_EXT_ID/g, VOCUS_EXT_ID));
	});
}

function bootstrapVocusScript(name)
{
	console.log("[Vocus] Loading " + name);
	
	var j = document.createElement('script');
	j.src = chrome.extension.getURL(name);
	(document.head || document.documentElement).appendChild(j);
}

function bootstrapVocusCss(name)
{
	console.log("[Vocus] Loading " + name);
	
	var s = document.createElement('link');
	s.rel = "stylesheet";
	s.type = "text/css";
	s.href = chrome.extension.getURL(name);
	(document.head || document.documentElement).appendChild(s);
}

function bootstrapVocusVar(name, value)
{
	var script = document.createElement('script');
	script.appendChild(document.createTextNode('var ' + name + ' = "'+ value +'";'));
	(document.head || document.documentElement).appendChild(script);
}