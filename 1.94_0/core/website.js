var VocusWebsiteBootstrap = {
	init: function()
	{
		VocusWebsiteBootstrap.addVar("VOCUS_EXT_ID", chrome.runtime.id);
		VocusWebsiteBootstrap.addScript("core/pubsub.js");
	},

	addScript: function(name)
	{
		var j = document.createElement('script');
		j.src = chrome.extension.getURL(name);
		(document.head || document.documentElement).appendChild(j);
	},

	addVar: function(name, value)
	{
		var script = document.createElement('script');
		script.appendChild(document.createTextNode('var ' + name + ' = "'+ value +'";'));
		(document.head || document.documentElement).appendChild(script);
	}
};

VocusWebsiteBootstrap.init();