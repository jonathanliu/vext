var PubsubServer =
{
	services: [],

	print: function(msg)
	{
		console.log("[PubsubServer] " + msg);
	},

	listen: function()
	{
		PubsubServer.print("Listening...");
		
		chrome.runtime.onConnect.addListener(PubsubServer.onConnect);
		chrome.runtime.onConnectExternal.addListener(PubsubServer.onConnect);
	},

	onConnect: function(port)
	{
		port.onMessage.addListener(PubsubServer.onMessage);
		port.onDisconnect.addListener(PubsubServer.onDisconnect);

		PubsubServer.services.push({
			name: port.name,
			port: port,
			local: false
		});

		PubsubServer.print("Received connection: " + port.name);
	},

	onDisconnect: function(port)
	{
		var svc = underscore.filter(PubsubServer.services, function(i) { return i.port == port; });
		PubsubServer.services = underscore.difference(PubsubServer.services, svc);
		
		PubsubServer.print("Lost connection: " + port.name);
	},

	onMessage: function(data)
	{
		// ignore pings
		if (data == "ping")
			return;

		// forward this message to every other service
		for(var i = 0; i < PubsubServer.services.length; i++)
		{
			var svc = PubsubServer.services[i];

			try
			{
				if (svc.local)
					svc.service.onMessage(data);
				else
					svc.port.postMessage(data);
			}
			catch (e)
			{
				PubsubServer.print("Error posting message to (onMessage): " + svc.name);

				// remove disconnected ports
				var index = e.toString().indexOf("disconnected port object");
				
				if (index > -1) {
					PubsubServer.removeService(svc.port);
				}
			}
			
		}
	},

	getService: function(name)
	{
		if (typeof name == "undefined")
			return null;

		for(var i = 0; i < PubsubServer.services.length; i++)
			if (PubsubServer.services[i].name == name)
				return PubsubServer.services[i];

		return null;
	},

	removeService: function(port)
	{
		var svc = underscore.filter(PubsubServer.services, function(i) { return i.port == port; });
		PubsubServer.services = underscore.difference(PubsubServer.services, svc);
	},

	addLocalService: function(name, service)
	{
		PubsubServer.services.push({
			name: name,
			service: service,
			local: true
		});
	},

	broadcastMessage: function(msg)
	{
		for(var i = 0; i < PubsubServer.services.length; i++)
		{
			var svc = PubsubServer.services[i];

			if (svc.local == false)
			{
				try
				{
					svc.port.postMessage(msg);
				}
				catch(e)
				{
					PubsubServer.print("Error posting message to (broadcastMessage): " + svc.name);
				}
			}
		}
	}
};

var PubsubClient =
{
	name: null,
	local: false,
	port: null,
	service: null,
	callbacks: [],
	
	keepConnection: true,
	pingTick: null,

	print: function(msg)
	{
		console.log("[PubsubClient] " + msg);
	},

	initService: function(name, service, local)
	{
		PubsubClient.print("Initialized service " + name);

		if (typeof underscore === "undefined")
		{
			window.underscore = _;
		}

		PubsubClient.name = name;
		PubsubClient.local = local;
		PubsubClient.service = service;

		if (!local)
		{
			PubsubClient.connect();
		}
	},

	connect: function()
	{
		var args = {name: PubsubClient.name};

		if (typeof VOCUS_EXT_ID != "undefined")
			PubsubClient.port = chrome.runtime.connect(VOCUS_EXT_ID, args);
		else
			PubsubClient.port = chrome.runtime.connect(args);

		PubsubClient.port.onMessage.addListener(PubsubClient.onMessage);

		// HAAACK: connection keeps dropping, so start interval to test connection
		clearInterval(PubsubClient.pingTick);
		PubsubClient.pingTick = setInterval(PubsubClient.ping, 3*1000);
	},

	disconnect: function()
	{
		PubsubClient.keepConnection = false;
		PubsubClient.port.disconnect();
	},

	ping: function()
	{
		if (!PubsubClient.keepConnection)
			return;

		try
		{
			PubsubClient.port.postMessage("ping");
		}
		catch(e)
		{
			PubsubClient.print("Lost connection with PubsubServer - reconnecting");
			PubsubClient.connect();
		}
	},

	call: function(service, name, args, callback)
	{
		var data = {
			action: "function-call",
			service: service,
			name: name,
			args: args,
			id: Math.random()
		};

		PubsubClient.registerCallback(data.id, callback);
		PubsubClient.postResult(data);
	},

	async: function(service, name, args, callback)
	{
		var data = {
			action: "async-call",
			service: service,
			name: name,
			args: args,
			id: Math.random()
		};

		PubsubClient.registerCallback(data.id, callback);
		PubsubClient.postResult(data);
	},

	get: function(service, attr, callback)
	{
		var data = {
			action: "attr-call",
			service: service,
			attr: attr,
			id: Math.random()
		};

		PubsubClient.registerCallback(data.id, callback);
		PubsubClient.postResult(data);
	},

	subscribe: function(name, callback)
	{
		PubsubClient.registerCallback("event-" + name, callback);
	},

	publish: function(name, args)
	{
		PubsubClient.postResult({
			id: "event-" + name,
			action: "event",
			result: args
		});
	},

	registerCallback: function(id, callback)
	{
		PubsubClient.callbacks.push({
			id: id,
			callback: callback
		});
	},

	onMessage: function(data)
	{
		if (data.action == "callback")
			PubsubClient.processCallback(data);
		else if (data.action == "function-call")
			PubsubClient.processFunctionCall(data);
		else if (data.action == "async-call")
			PubsubClient.processAsyncCall(data);
		else if (data.action == "attr-call")
			PubsubClient.processAttrCall(data);
		else if (data.action == "event")
			PubsubClient.processEvent(data);
	},

	processCallback: function(data)
	{
		var cb = underscore.find(PubsubClient.callbacks, function(i) { return i.id == data.id; });

		if (cb && cb.callback)
		{
			cb.callback(data.result);
			PubsubClient.callbacks = underscore.without(PubsubClient.callbacks, cb);
		}
	},

	processEvent: function(data)
	{
		var cbs = underscore.filter(PubsubClient.callbacks, function(i) { return i.id == data.id; });
	
		for (var i = 0; i < cbs.length; i++)
		{
			var cb = cbs[i];

			if (cb.callback)
				cb.callback(data.result);
		}
	},

	processFunctionCall: function(data)
	{
		if (data.service != PubsubClient.name)
			return;

		var args = Array.isArray(data.args) ? data.args : [data.args];
		var result = PubsubClient.service[data.name].apply(PubsubClient.service, args);

		PubsubClient.postResult({
			action: "callback",
			id: data.id,
			result: result
		});
	},

	processAsyncCall: function(data)
	{
		if (data.service != PubsubClient.name)
			return;

		var args = Array.isArray(data.args) ? data.args : [data.args];
		
		args.push(function(result) {

			PubsubClient.postResult({
				action: "callback",
				id: data.id,
				result: result
			});

		});

		PubsubClient.service[data.name].apply(PubsubClient.service, args);
	},

	processAttrCall: function(data)
	{
		if (data.service != PubsubClient.name)
			return;

		var result = PubsubClient.service[data.attr];

		PubsubClient.postResult({
			action: "callback",
			id: data.id,
			result: result
		});
	},

	postResult: function(data)
	{
		if (!PubsubClient.local)
			PubsubClient.port.postMessage(data);
		else
		{
			PubsubServer.broadcastMessage(data);
			PubsubClient.onMessage(data);
		}
	}
};


