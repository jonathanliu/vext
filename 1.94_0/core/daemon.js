var VocusDaemon =
{
	profile: {
		auth: false,
		disabled: true,
		userId: "",
		email: "",
		name: "",
		secret: "",
		serverMessage: "",
		snoozes: [],
		accounts: [],
		streamServer: ""
	},

	lastDraftSave: {},
	silenceAll: false,
	silencedMessages: [],

	init: function()
	{
		VocusDaemon.print("Daemon loaded.");

		// load settings
		VocusDaemon.silenceAll = VocusDaemon.getSettings("silenceAll", false);
		VocusDaemon.silencedMessages = VocusDaemon.getSettings("silencedMessages", []);
		
		// sync
		VocusDaemon.sync();

		// reload Gmail
		chrome.tabs.query({url: "https://mail.google.com/*"}, function(result) {
			var isGmailOpen = false;
			
			for(var i = 0; i < result.length; i++)
			{
				chrome.tabs.reload(result[i].id);
				isGmailOpen = true;
			}

			// open gmail if not already open (only the first 3 times the ext is enabled)
			var timesOpened = typeof VocusDaemon.getSettings("timesGmailOpened") == "undefined" ? 0 : VocusDaemon.getSettings("timesGmailOpened");

			if (!isGmailOpen && timesOpened < 3)
			{
				VocusDaemon.setSettings("timesGmailOpened", timesOpened+1);
				chrome.tabs.create({ url: "https://mail.google.com/mail/u/0/" });
			}

		});

		// open dashboard-v2
		chrome.browserAction.onClicked.addListener(VocusDaemon.toolbarClicked);

		// record uninstalls
		chrome.runtime.setUninstallURL(VOCUS_SERVER + "/uninstalled");

		// listen to messages
		PubsubServer.listen();
		PubsubClient.initService("VocusDaemon", VocusDaemon, true);
		PubsubServer.addLocalService("VocusDaemon", PubsubClient);
		
		// silence current-user trackers
		chrome.webRequest.onBeforeSendHeaders.addListener(VocusDaemon.trackerFilter, {urls: [ "*://*.googleusercontent.com/proxy/*" ]}, ["requestHeaders","blocking"]);
		chrome.webRequest.onBeforeSendHeaders.addListener(VocusDaemon.trackerFilter, {urls: [ "*://*.vocus.io/hello.png" ]}, ["requestHeaders","blocking"]);
		
		// intercept dashboard
		chrome.webRequest.onBeforeRequest.addListener(VocusDaemon.onLoadingDashboard, {urls: [ "*://*.vocus.io/*" ], types: ["sub_frame"]}, ["requestBody", "blocking"]);
		chrome.webRequest.onCompleted.addListener(VocusDaemon.onLoadedDashboard, {urls: [ "*://*.vocus.io/*" ], types: ["sub_frame"]}, ["responseHeaders"]);

		// intercept new emails
		chrome.webRequest.onBeforeRequest.addListener(VocusDaemon.sendMessageFilterBefore, {urls: [ "*://mail.google.com/*" ], types: ["xmlhttprequest"]}, ["requestBody", "blocking"]);
		chrome.webRequest.onCompleted.addListener(VocusDaemon.sendMessageFilterAfter, {urls: [ "*://mail.google.com/*" ], types: ["xmlhttprequest"]}, ["responseHeaders"]);

		// bind notification buttons
		chrome.notifications.onButtonClicked.addListener(VocusDaemon.notificationClicked);
	},

	//
	// onsend filters

	sendMessageFilterBefore: function(req)
	{
		if (req.url.indexOf("/i/s?") == -1)
			return;

		var data = VocusDaemon.getDataFromRequest(req);

		if (data.isEmailSend)
		{
			// return {redirectUrl: req.url.replace("/i/s", "/i/fd")}
			// console.log("Email Send", data);
		}
		else if (data.hasEmailBody)
		{
			// remember draft-save details in order to publish later on
			VocusDaemon.lastDraftSave = {
				requestId: req.requestId,
				threadId: data.threadId,
				msgId: data.msgId
			}

			// announce it
			PubsubClient.publish("draft-saving", VocusDaemon.lastDraftSave);

			// FYI, another way to detect OnDraftSave:
			//   - Gmail DOM, Compose (.M9) has many `input` with `type=hiddenn`.
			//   - One of those inputs represents the content of the email body.
			//   - Value of that input is updated after DraftSave.
		}
	},

	sendMessageFilterAfter: function(req)
	{
		if (req.url.indexOf("/i/s?") == -1)
			return;
		
		// result of draft-save? announce it
		if (VocusDaemon.lastDraftSave.requestId == req.requestId)
		{
			PubsubClient.publish("draft-saved", VocusDaemon.lastDraftSave);
		}
	},

	getDataFromRequest: function(req)
	{
		var data = {};

		try
		{
			var buf = req.requestBody.raw[0].bytes;
			var form = String.fromCharCode.apply(null, new Uint8Array(buf));
			data = JSON.parse(form);

			// var email = VocusDaemon.getEmailData(data);

			// has email body? we expect 'msg-a' to exist, not inside a array
			if (form.indexOf(":\"msg-a:r") > -1)
			{
				data.hasEmailBody = true;
			}
			else
			{
				data.hasEmailBody = false;
			}

			// is email send? we expect 'data[1]:{3:2}' to exist, along with body
			if (JSON.stringify(data["1"]) == JSON.stringify({3:2}) && data.hasEmailBody)
			{
				data.isEmailSend = true;
			}
			else
			{
				data.isEmailSend = false;
			}

			// get aux data
			if (data.hasEmailBody)
			{
				// try to get thread-id
				try { data.threadId = data[2][1][0][2][1] } catch(e) {  }

				// try to get msg-id (happens on subsequent draft save)
				try { data.msgId = data[2][1][0][2][2][14][1][1] } catch(e) {  }
			}

			// mark it as successful parse
			data.success = true;
		}
		catch(e)
		{
			data = {"success": false, "error": "failed to parse form data."}
		}

		return data;
	},

	//
	// Extract Email Data

	getEmailData: function(obj)
	{
		if (!obj || typeof obj != "object")
			return null;

		for(key in obj)
		{
			if (key == "1" && typeof obj[key] == "string" && /^msg-a:/.test(obj[key]) && Object.keys(obj).length >= 5)
			{
				var email = {};

				try { email.email_id = obj[1]; } catch(e) { /* none */ }
				try { email.subject = obj[8]; } catch(e) { /* none */ }
				try { email.from = obj[2]; } catch(e) { /* none */ }
				try { email.to = obj[3]; } catch(e) { /* none */ }
				try { email.cc = obj[4]; } catch(e) { /* none */ }
				try { email.bcc = obj[5]; } catch(e) { /* none */ }
				try { email.body = VocusDaemon.getEmailDataBody(obj); } catch(e) { /* none */ }
				try { email.labels = obj[11]; } catch(e) { /* none */ }

				return email;
			}
			else if (typeof obj[key] == "object")
			{
				var result = VocusDaemon.getEmailData(obj[key]);

				if (result != null)
					return result;
			}
		}

		return null;
	},

	getEmailDataBody: function(obj)
	{
		var body = null;

		for(var section in obj[9][2])
		{
			body = (body || "") + obj[9][2][section][2];
		}

		return body;
	},

	getLabelsData: function(obj)
	{
		if (!obj || typeof obj != "object")
			return null;

		for(key in obj)
		{
			var isLabels = true;
			isLabels = isLabels && Array.isArray(obj[key]);
			isLabels = isLabels && underscore.every(obj[key], (item) => { return (typeof item == "string") && (item.indexOf("^") == 0); });
			
			if (isLabels)
			{
				return obj[key];
			}
			else if (typeof obj[key] == "object")
			{
				var result = VocusDaemon.getLabelsData(obj[key]);

				if (result != null)
					return result;
			}
		}

		// FYI "^pfg" label means "send_message"

		return null;
	},

	//
	// Tracker filter

	trackerFilter: function(req)
	{
		if (!req.url.includes("/hello.png"))
			return;

		if (VocusDaemon.profile == null || VocusDaemon.profile.accounts == null)
			return;
		
		var found = VocusDaemon.profile.accounts.filter(function(acc) { return req.url.includes(acc.email) }).length > 0;
		
		if (found)
		{
			VocusDaemon.print("Blocked own tracker - " + req.url);
			return {cancel: true};
		}
	},

	//
	// Dashboard events

	onLoadingDashboard: function(req)
	{
		PubsubClient.call("VocusFrame", "onLoading", null, null);
	},

	onLoadedDashboard: function(req)
	{
		setTimeout(function() {
			PubsubClient.call("VocusFrame", "onLoaded", null, null);
		}, 1000);
	},

	//
	// Auth events

	setProfile: function(profile)
	{
		if (profile.auth == false)
		{
			VocusDaemon.profile.auth = false;
			VocusDaemon.profile.serverMessage = null;
			VocusDaemon.profileChanged();
			return;
		}

		// first session?
		if (VocusDaemon.profile.auth == false)
		{
			VocusDaemon.startSession();
		}

		// details
		VocusDaemon.profile.auth = true;
		VocusDaemon.profile = underscore.extend(VocusDaemon.profile, profile);
		
		// announce changes
		VocusDaemon.profileChanged();
	},

	profileChanged: function()
	{
		PubsubClient.publish("profile-changed", VocusDaemon.profile);
		VocusDaemon.refreshStream();
	},

	sync: function()
	{
		VocusDaemon.print("Syncing.");
		var version = chrome.runtime.getManifest()["version"];
		
		$.get(VOCUS_SERVER + "/sync", {version: version}, function(result)
		{
			VocusDaemon.setProfile(result);
		});
	},

	startSession: function()
	{
		VocusDaemon.print("Beginning session.");
		PubsubClient.publish("session-started", {});
	},

	//
	// Stream & Notification Operations

	refreshStream: function()
	{
		// do nothing if no stream server defined
		if (VocusDaemon.profile.streamServer == "" || typeof VocusDaemon.profile.streamServer == "undefined")
		{
			VocusDaemon.print("No stream server found.");
			return;
		}

		// disconnect if not authenticated
		if (VocusDaemon.profile.auth == false)
		{
			if (VocusDaemon.client != null)
				VocusDaemon.client.disconnect();

			VocusDaemon.client = null;
			VocusDaemon.print("Disconnecting stream.");
			return;
		}

		// only initialize once
		if (VocusDaemon.client != null)
			return;

		VocusDaemon.client = new Faye.Client(VocusDaemon.profile.streamServer, {retry: 30});
		Faye.URI.isSameOrigin = function(host){return true};

		var channel = "/" + VocusDaemon.profile.userId + "-" + VocusDaemon.profile.secret;
		var subscription = VocusDaemon.client.subscribe(channel, VocusDaemon.streamHandler);

		VocusDaemon.print("Connecting stream.");
	},

	streamHandler: function(message)
	{
		// silence individual
		if (message.aux.id != null && VocusDaemon.silencedMessages.indexOf(String(message.aux.id)) != -1)
			return;

		// silence all
		if (VocusDaemon.silenceAll)
			return;

		// duplicate
		if (VocusDaemon.lastNotification)
		{
			var diff = (new Date() - VocusDaemon.lastNotification.date) / 1000;

			// duplicate entire message (10 seconds)
			var newStr = JSON.stringify(message);
			var oldStr = VocusDaemon.lastNotification.content;
			if (newStr == oldStr && diff < 10)
				return;

			// duplicate message title only (2 seconds)
			var newObj = message;
			var oldObj = JSON.parse(VocusDaemon.lastNotification.content);
			if (newObj.title == oldObj.title && diff < 2)
				return;
		}

		VocusDaemon.showNotification(String(message.aux.id), message.title, message.body);
		VocusDaemon.lastNotification = {content: JSON.stringify(message), date: new Date()};
		VocusDaemon.sync();
	},

	silenceMessage: function(id, silence)
	{
		if (silence)
		{
			VocusDaemon.silencedMessages.push(id);
		}
		else
		{
			VocusDaemon.silencedMessages = underscore.without(VocusDaemon.silencedMessages, id);
		}

		VocusDaemon.setSettings("silencedMessages", VocusDaemon.silencedMessages);
		PubsubClient.publish("silenced-messages-changed", VocusDaemon.silencedMessages);
	},

	silenceAllMessages: function(flag)
	{
		VocusDaemon.silenceAll = flag;
		VocusDaemon.setSettings("silenceAll", flag);
	},

	showNotification: function(id, title, body)
	{
		chrome.notifications.create(id, {
			type: "basic",
			title: title,
			message: body,
			iconUrl: "../images/logo-icon-rect.png",
			isClickable: true,
			buttons: [{title:"Mute Thread"}, {title:"Mute All Threads"}]
		}, null);
	},

	notificationClicked: function(notificationId, btnId)
	{
		// mute thread
		if (btnId == 0)
		{
			VocusDaemon.silenceMessage(notificationId, true);
		}

		// mute all
		else if (btnId == 1)
		{
			VocusDaemon.silenceAllMessages(true);
		}

		chrome.notifications.clear(notificationId);
	},

	//
	// Settings Helper

	getSettings: function(name, defaultValue)
	{
		var val = localStorage.getItem(name);
		
		if (val == null)
			return defaultValue;

		return JSON.parse(val);
	},

	setSettings: function(name, value)
	{
		localStorage.setItem(name, JSON.stringify(value));
	},

	//
	// HTTP Helpers, used to workaround some content-script policy restrictions (although we might be able to get rid off now that we are using InboxSDK)

	getHTTP: function(path, params, callback)
	{
		if (params == null || typeof params == "undefined")
			params = {};

		params.version = chrome.runtime.getManifest()["version"];

		$.get(path, params, function(result)
		{
			if (callback)
				callback(result);
		});
	},

	postHTTP: function(path, params, callback)
	{
		if (params == null || typeof params == "undefined")
			params = {};

		params.version = chrome.runtime.getManifest()["version"];

		$.post(path, params)
			
		.done(function(result)
		{
			if (callback)
				callback(result);
		})

		.fail(function(e) {
			if (callback)
				callback(e);
		});
	},

	//
	// Misc.

	signout: function()
	{
		$.get(VOCUS_SERVER + "/users/sign_out", function() {
			VocusDaemon.sync();
		});
	},

	print: function(msg)
	{
		console.log("[Vocus] " + msg);
	},

	toolbarClicked: function()
	{
		// dashboard-v1
		if (!VocusDaemon.profile.enableNewDashboard)
		{
			chrome.tabs.create({url: "https://vocus.io/welcome-to-vocus"});
			return;
		}

		// dashboard-v2
		var url = VOCUS_SERVER + "/dashboard";

		chrome.tabs.query({url: url}, function(result) {
			if (result.length == 0)
			{
				chrome.tabs.create({url: url});
			}
			else
			{
				var last = result[result.length - 1];
				chrome.tabs.reload(last.id);
				chrome.tabs.update(last.id, {active: true}, (tab) => {});
			}
		});
	}
};

VocusDaemon.init();


