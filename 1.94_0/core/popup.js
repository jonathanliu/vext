var VocusPopup = {

	settings: null,

	init: function() {
		// prepare overlay
		VocusPopup.overlay = jQuery("#vocus-overlay");
		VocusPopup.overlay.on("click", VocusPopup.close);

		// prepare content
		VocusPopup.container = jQuery("#vocus-container");
		VocusPopup.frame = jQuery("#vocus-frame");
	},

	open: function(settings) {
		// apply dimensions
		VocusPopup.settings = settings;
		VocusPopup.applyDimensions();
		
		// make visible
		PubsubClient.publish("frame-visibility-changed", {});
		VocusPopup.overlay.fadeIn();

		setTimeout(function() {
			VocusPopup.container.addClass("visible");
		},200);

		var hash = btoa(JSON.stringify(VocusPopup.settings));
		var url = "chrome-extension://" + VOCUS_EXT_ID + "/html/frame.html#" + hash;
		VocusPopup.frame.attr("src", url);
	},

	close: function() {
		VocusPopup.container.removeClass("visible");
		PubsubClient.publish("frame-visibility-changed", {});

		setTimeout(function() {
			VocusPopup.overlay.fadeOut();
		},200);

		VocusPopup.frame.attr("src", "");
	},

	applyDimensions: function()
	{
		var borderRadius = VocusPopup.settings.borderRadius ? VocusPopup.settings.borderRadius : "5px";

		VocusPopup.container.css({ 
			width: VocusPopup.settings.width, 
			height: VocusPopup.settings.height,
			marginLeft: -VocusPopup.settings.width/2,
			borderRadius: borderRadius
		});
		
		VocusPopup.frame.css({ 
			width: VocusPopup.settings.width, 
			height: VocusPopup.settings.height,
			borderRadius: borderRadius
		});
	}
};

// setTimeout(VocusPopup.init, 2*1000);