var VocusFeatures =
{
	auth: false,
	profile: null,
	settings: {},
	iSDK: null,
	context: Math.ceil(Math.random()*1000000),
	initialized: false,

	activeThreadId: null,
	activeThreadSubject: null,

	init: function()
	{
		// avoid double initialization
		if (VocusFeatures.initialized) return;
		VocusFeatures.initialized = true;

		// report init progress
		VocusFeatures.print("Ready.");
		PubsubClient.initService("VocusFeatures", VocusFeatures, false);

		// initialize inboxsdk
		InboxSDK.load(1, "sdk_vocusio_06b2054492").then(function(sdk){

			// make the iSDK available to other features
			VocusFeatures.iSDK = sdk;

			// determine & listen to changes in auth status
			PubsubClient.get("VocusDaemon", "profile", VocusFeatures.profileChanged);
			PubsubClient.subscribe("profile-changed", VocusFeatures.profileChanged);

			// listen to hashchange
			window.addEventListener("hashchange", VocusFeatures.hashChanged, false);
			PubsubClient.subscribe("hash-changed", VocusFeatures.processPendingRefresh);

			// listen to thread change
			sdk.Conversations.registerThreadViewHandler(VocusFeatures.threadViewChanged);

			// initialize features
			VocusCPanel.init();
			VocusCompose.init();
			VocusCompletion.init();
			VocusSnippets.init();
			VocusSendlater.init();
			VocusDashboard.init();
			VocusFollowup.init();
			VocusMeeting.init();
			VocusPoll.init();
			VocusSnooze.init();
			VocusToast.init();
			VocusTBPanel.init();
			VocusTracking.init();
			VocusUndo.init();
			VocusPopup.init();
			VocusDrafts.init();
			VocusCheckmarks.init();
		});
	},

	print: function(msg)
	{
		console.log("[Vocus Features] " + msg);
	},

	load: function(name)
	{
		var url = "chrome-extension://" + VOCUS_EXT_ID + "/features/" + name;

		jQuery.get(url).done(function(content) {
			var html = content.replace(/VOCUS_EXT_ID/g, VOCUS_EXT_ID);
			$("body").prepend(html);
		});
	},

	popup: function(url, title, width, height)
	{
		var left = (document.body.clientWidth/2)-(width/2);
		var top = (document.body.clientHeight/2)-(height/2) + 60;

		if (navigator.platform.indexOf("Win") > -1)
			width += 30;

		return window.open(url, title, 'toolbar=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
	},

	hashChanged: function()
	{
		PubsubClient.publish("hash-changed", null);
	},

	refresh: function()
	{
		var btn = null;
		
		// get refresh btn using title (reliable even when Gmail changes their UI)
		btn = $("div[title='Refresh']:visible");

		// not found? search by data attributes
		if (btn.length == 0)
			btn = $("div[data-tooltip='Refresh']:visible");

		// not found? search by CSS class (used when UI is not English)
		if (btn.length == 0)
			btn = $(".asf:visible").parents("div[role=button]");

		// still not found? then fail
		if (btn.length == 0)
		{
			VocusFeatures.print("Failed to refresh.");
			VocusFeatures.pendingRefresh = true;
			return;
		}

		// remember CSS class names
		var classes = btn.attr("class");

		// call it
		VocusFeatures.simulateClick(btn.get(0));

		// restore CSS class names (in order to remove button highlight)
		btn.attr("class", classes);

		// mark it as done
		VocusFeatures.print("Refreshing.");
		VocusFeatures.pendingRefresh = false;
	},

	processPendingRefresh: function()
	{
		// Used when refresh failes because Refresh button was not created.
		// This happens when Gmail is loaded directly to a message view, without passing inbox.

		if (!VocusFeatures.pendingRefresh)
			return;

		if (VocusFeatures.isInsideEmail())
			return;

		VocusFeatures.print("Processing pending refresh.");
		setTimeout(VocusFeatures.refresh, 200);
	},

	threadViewChanged: function(view)
	{
		// record thread id and subject lines for use by other components, such as Snooze 
		view.getThreadIDAsync().then((id) => { VocusFeatures.activeThreadId = id; }); // we use getThreadIDAsync on iSDK because getThreadID is deprecated
		VocusFeatures.activeThreadSubject = view.getSubject();

		view.on("destroy", function() {
			VocusFeatures.activeThreadId = null;
			VocusFeatures.activeThreadSubject = null;
		});
	},

	isInsideEmail: function()
	{
		// get address value between hash and question mark
		var hash = window.location.hash.split('#').pop().split('?').shift();
		var slashes = hash.split("/");

		// no slashes? index
		if (slashes.length == 1) return false;

		// from within a label
		if (hash.startsWith("label/") || hash.startsWith("search/"))
		{
			if (slashes.length != 3) return false;
			if (slashes[2].startsWith("p")) return false;
			return true;
		}

		// pagination of an index page
		if (slashes.length == 2 && slashes[1].startsWith("p")) return false;
		return true;
	},

	isConversationView: function()
	{
		var flag = true;

		try { flag = !underscore(GLOBALS[17][4][1]).any((i) => i[0] == "bx_vmb" && i[1] == "1"); }
		catch (e) { flag = true; }

		return flag;
	},

	isAccount: function(email)
	{
		if (VocusFeatures.profile.accounts == null)
			return true;
		
		return VocusFeatures.profile.accounts.filter(function(acc) { return acc.email != null && acc.email.toLowerCase() === email.toLowerCase() }).length > 0;
	},

	alert: function(data)
	{
		VocusAlert.alert(data);
	},

	dialog: function(data)
	{
		VocusAlert.dialog(data);
	},

	profileChanged: function(profile)
	{
		VocusFeatures.auth = profile.auth;
		VocusFeatures.profile = profile;

		// settings
		if (VocusFeatures.profile.accounts != null)
		{
			var email = VocusFeatures.iSDK.User.getEmailAddress();
			var settings = VocusFeatures.profile.accounts.filter(function(acc) { return acc.email === email });

			VocusFeatures.settings = settings.length > 0 ? settings[0] : {};
		}
	},

	getPopupSettings: function()
	{
		return VocusPopup.settings;
	},

	getEmailAddress: function()
	{
		return VocusFeatures.iSDK.User.getEmailAddress();
	},

	dashboardOpen: function()
	{
		if (VocusDashboard.isVisible)
		{
			VocusDashboard.refresh();
		}
		else
		{
			VocusDashboard.open();
		}
	},

	dashboardRefresh: function()
	{
		if (VocusDashboard.isVisible)
		{
			VocusDashboard.refresh();
		}
	},

	dashboardClose: function()
	{
		VocusDashboard.close();
	},

	popupClose: function()
	{
		VocusPopup.close();
	},

	simulateClick: function(elem)
	{
		// helper function, simulate a button click
		elem.dispatchEvent(new MouseEvent('mousedown'));
		elem.dispatchEvent(new MouseEvent('mouseup'));
	},

	forceRenderUI: function()
	{
		// force Gmail to re-render UI with the available data by temporarily changing the hash
		var loc = window.location.hash;
		window.location.hash = "#";
		setTimeout(function() { window.location.hash = loc; }, 0);
		// setTimeout(function() { PubsubClient.publish("hash-changed", null); }, 500); // necessary to re-render toolbar button
	},

	openLastDraft: function()
	{
		VocusFeatures.iSDK.Router.goto("drafts/:page");
	},

	openDraft: function(promise)
	{
		promise.then((id) => {
			console.log(id);
			VocusFeatures.iSDK.Compose.openDraftByMessageID(id);
		});
	},

	enableNewDashboard: function() // remove this once DashboardV2 is rolled out and becomes the default
	{
		return VocusFeatures.profile && VocusFeatures.profile.enableNewDashboard;
	}
};

// Initialize
// For now, we need to wrap it in a window.onload + setTimeout to build around an error msg by iSDK.
// In the future we can test calling VocusFeatures.init directly, or at least upon window load.
window.addEventListener("load", () => {
	setTimeout(() => {
		VocusFeatures.init();
	}, 2000)
});

VocusFeatures.init();