var VocusFrame = {
    init: function() {
        // join the conversation
        PubsubClient.initService("VocusFrame", VocusFrame);

        PubsubClient.subscribe("frame-hash-changed", VocusFrame.hashChanged);
        VocusFrame.hashChanged();
    },

    onLoaded: function() {
        VocusFrame.loading.removeClass("visible");
        //VocusFrame.loading.fadeOut();
    },

    onLoading: function() {
        VocusFrame.loading.addClass("visible");
        //VocusFrame.loading.fadeIn();
    },

    hashChanged: function() {
        // get settings
        var settings = JSON.parse(atob(location.hash.replace("#", "")));

        // get url
        var url = VOCUS_SERVER;
        
        if (url.indexOf("https") == -1)
            url = url.replace("http", "https");
                
        // apply properties
        url = url + settings.url;
        
        // prepare loading
        VocusFrame.loading = $("#vocus-loading");
        VocusFrame.loadingIcon = $("#vocus-loading-icon");
        VocusFrame.loadingIcon.css({marginTop: settings.height / 2 - 15})

        // prepare frame
        VocusFrame.frame = $("#vocus-frame");
        VocusFrame.frame.css({width: settings.width, height: settings.height});
        VocusFrame.frame.attr("src", url);
    }
};

$(VocusFrame.init);