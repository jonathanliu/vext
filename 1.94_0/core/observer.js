/*
	Helper.
	A less verbose way to attache mutation observers on DOMs.

	Example:

	DomObserver.attach_css("foo:first", function(mut) {
		console.log("Changed: " + mut);
	})
*/
var DomObserver =
{
	attach: function(selector, options, handler)
	{
		// make sure it's onl one element
		var element = $(selector);

		if (element.length > 1)
		{
			console.log("[DOM Observer] Selector (" + selector + ") should return single element only. Taking first.");
		}

		// prepare mutation observer obj
		var observer = new MutationObserver(function(mut) { 
			mut.forEach(function(m) {
				handler(m);
			});
		});

		// attach it!
		observer.observe(element[0], options);

		// return it, in case you want to observer.disconnect();
		return observer;
	},

	attach_css: function(selector, handler)
	{
		DomObserver.attach(selector, { attributes: true, attributeFilter: ["style", "class"] }, handler);
	},

	attach_children: function(selector, handler)
	{
		DomObserver.attach(selector, { childList : true }, handler);
	}
}